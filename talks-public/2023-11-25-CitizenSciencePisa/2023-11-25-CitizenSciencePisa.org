#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage: key infrastructure for Open Science
# #+AUTHOR: Roberto Di Cosmo
# #+EMAIL: roberto@dicosmo.org @rdicosmo @swheritage
#+BEAMER_HEADER: \date[25/11/2023]{25 November 2023}
#+BEAMER_HEADER: \title[Software Heritage as a key infrastructure]{Software Heritage: key infrastructure for Open Science and Open Source}
#+BEAMER_HEADER: \author[R. Di Cosmo~~~~ roberto@dicosmo.org ~~~~ (CC-BY 4.0)]{Roberto Di Cosmo}
#+BEAMER_HEADER: \institute[Software Heritage]{Director, Software Heritage\\Inria and Universit\'e de Paris Cit\'e}
# #+BEAMER_HEADER: \setbeameroption{show notes on second screen}
#+BEAMER_HEADER: \setbeameroption{hide notes}
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology
#+LATEX_HEADER: \usepackage{tcolorbox}\usepackage{fontawesome5}
#+LATEX_HEADER: \definecolor{links}{HTML}{2A1B81}
#+LATEX_HEADER: \definecolor{links}{HTML}{0ADB11}
#+LATEX_HEADER: \hypersetup{colorlinks,linkcolor=,urlcolor=links}
#+LATEX_HEADER: \hypersetup{colorlinks,linkcolor=,urlcolor=cyan}

#
# prelude.org contains all the information needed to export the main beamer latex source
# use prelude-toc.org to get the table of contents
#

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1


#+INCLUDE: "../../common/modules/169.org"

# +LaTeX_CLASS_OPTIONS: [aspectratio=169,handout,xcolor=table]

#+LATEX_HEADER: \usepackage{bbding}
#+LATEX_HEADER: \DeclareUnicodeCharacter{66D}{\FiveStar}

#
# If you want to change the title logo it's here
#
# +BEAMER_HEADER: \titlegraphic{\includegraphics[width=0.5\textwidth]{SWH-logo}}

# aspect ratio can be changed, but the slides need to be adapted
# - compute a "resizing factor" for the images (macro for picblocks?)
#
# set the background image
#
# https://pacoup.com/2011/06/12/list-of-true-169-resolutions/
#
#+BEAMER_HEADER: \pgfdeclareimage[height=90mm,width=160mm]{bgd}{swh-world-169.png}
#+BEAMER_HEADER: \setbeamertemplate{background}{\pgfuseimage{bgd}}
#+LATEX: \addtocounter{framenumber}{-1}
* Introduction
#+INCLUDE: "../../common/modules/rdc-bio.org::#main" :only-contents t :minlevel 2
** Software and Source code: a reminder
   #+INCLUDE: "../../common/modules/source-code-different-short.org::#thesourcecode" :only-contents t :minlevel 3
** Software /Source Code/ is Precious Knowledge
   #+INCLUDE: "../../common/modules/source-code-different-short.org::#softwareisdifferent" :only-contents t :minlevel 3
** ~ 50 years, a lightning fast growth
#+INCLUDE: "../../common/modules/50years-source-code.org::#apollolinux" :only-contents t :minlevel 3
** Free Software: 40 years, 4 layers, in a nutshell
*** Free Software, AKA: \emph{Open Source}, FOSS, FLOSS,\ldots
    Software that offers to \emph{its users} the freedom to:
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .5
     :END:
     - \structure{use} the software
     - \structure{study} and \structure{adapt} the software
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .5
     :END:
     - \structure{distribute} software copies
     - distribute \structure{modified copies}
*** vspace                                                     :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
   \vspace{-0.5em}
    #+BEAMER: \pause
*** First 15 years: 1984-... \hfill The early revolution                                                             :B_block:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
     - focus :: /freedom/ (users, *developers*)
     - keyword :: free software \hfill (individual)
     #+BEAMER: \pause
*** 1999-... \hfill Progressive industry adoption
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
    - focus :: software quality, reduced cost
    - keyword :: open source \hfill (entities)
#+BEAMER: \pause
*** vspace                                                     :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
   \vspace{-0.5em}
*** 2010-... \hfill Ecosystems, strategic alignment
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
    - focus :: organisation, foundations
    - keyword :: governance and funding
#+BEAMER: \pause
*** 2015-... \hfill Industry consolidation
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
    - focus :: mergers and acquisitions
    - keyword :: control 
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** 
     \hfill we faced many common issues */way before Open Science was on the radar/*
* Software source code for Open Science
** Why Open Science?
#+BEAMER: \vspace{-.5em}   
*** Open Science ([[https://www.ouvrirlascience.fr/wp-content/uploads/2021/10/Second_French_Plan-for-Open-Science_web.pdf][Second National Plan for Open Science]], France, 2021)
    /Unhindered/ dissemination of results, methods and products from scientific
    research.\\
    It draws on /the opportunity provided by recent digital progress/
    to develop /open access/ to /publications/ and – as much as possible – /data/,
    /source code/ and /research methods/.
#+BEAMER: \pause
#+BEAMER: \vspace{-.3em}   
*** Jean-Eric Paquet (EU DGRI, [[https://www.eosc.eu/sites/default/files/EOSC-SRIA-V1.0_15Feb2021.pdf][on the objective of Open Science]])
# Preface of EOSC SRIA https://www.eosc.eu/sites/default/files/EOSC-SRIA-V1.0_15Feb2021.pdf    
    “Increase /scientific quality/, the /pace of discovery and technological
    development/, as well as /societal trust in science/.”
#+BEAMER: \pause
#+BEAMER: \vspace{-.1em}   
*** Mariya Gabriel ([[https://www.s4d4c.eu/insights-from-commissioner-mariya-gabriel-towards-the-european-union-science-diplomacy/][EU Commissionneer]] for Research)
# From the article: https://www.s4d4c.eu/insights-from-commissioner-mariya-gabriel-towards-the-european-union-science-diplomacy/
    The COVID-19 crisis has also shown that cooperation at international level
    in research and innovation is more important than ever, including through
    /open access to data and results/. /No nation, no country can tackle any of
    these global challenges alone/.
#+BEAMER: \pause
#+BEAMER: \vspace{-.3em}   
*** Yuval Noah Harari (on COVID 19)
    \hfill /“The real antidote [to epidemic] is/ scientific knowledge /and/ global cooperation.”
** The scientific method...
   #+INCLUDE: "../../common/modules/scientific-method.org::#short" :only-contents t :minlevel 3
** ... updated for the digital age                              :skipinshort:
   #+INCLUDE: "../../common/modules/reprod-digital-age.org::#reprod" :only-contents t :minlevel 3
** Software is a pillar of Open Science
   #+INCLUDE: "../../common/modules/swh-ardc.org::#pillaropenscience_monitor" :only-contents t :minlevel 3
#+BEAMER: \pause   
*** 
    \hfill Preserving (the history of) source code is necessary for /reproducibility/
** Source code is /special/ (software is /not/ data)            :skipinshort:
  # Was: #+INCLUDE: "../../common/modules/swh-ardc.org::#swnotdata" :only-contents t :minlevel 3
*** Software /evolves/ over time
    - projects may last decades
    - the /development history/ is key to its /understanding/
#+BEAMER: \pause
*** Complexity :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=python3-matplotlib.pdf, width=.6\linewidth
    :END:
    - /millions/ of lines of code
    - large /web of dependencies/
      + easy to break, difficult to maintain
      + /research software/ a thin top layer
    - sophisticated /developer communities/
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** The human side
    design, algorithm, code, test, documentation, community, funding\\
    \hfill and so many more facets ...
** How are we managing our software ?                           :skipinshort:
*** Reproducibility, maintenance in Academia                        :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    #+ATTR_LATEX: :width .9\linewidth
    file:rsissues.pdf
    (articles: [[https://www.nature.com/articles/s41597-022-01143-6][here]], [[https://ieeexplore.ieee.org/document/8816763][here]], [[https://www.quantamagazine.org/crucial-computer-program-for-particle-physics-at-risk-of-obsolescence-20221201/][here]] and [[http://reproducibility.cs.arizona.edu/tr.pdf][here]])
    #+BEAMER: \pause
*** Security, integrity, traceability in Industry                   :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.85\linewidth]{supplychainwoes}
\end{center}
#+END_EXPORT
    Can they track the software that they
    - ship, use, acquire
    - has that bug or vulnerability
    #+BEAMER: \pause
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** 
    \hfill awareness is raising at the level of public policy
* An emerging policy framework                                  :skipinshort:
** International highlights
   #+INCLUDE: "../../common/modules/policyactions.org::#swpolicycompact2" :only-contents t :minlevel 3
** French National plan for Open Science, 2021-2024                
   #+INCLUDE: "../../common/modules/policyactions.org::#PNSO2-official" :only-contents t :minlevel 3
** Software College in the CoSO                                 :skipinshort:
   #+INCLUDE: "../../common/modules/policyactions.org::#collegelogiciel" :only-contents t :minlevel 3
*** Composition                                                  :skipinlong:
    Chairs: Roberto Di Cosmo and François Pellegrini\\
    20+ active members from a broad panel of institutions and fields
** Software College in the CoSO, cont'd                         :skipinshort:
   #+INCLUDE: "../../common/modules/policyactions.org::#collegelogicielmembers" :only-contents t :minlevel 3
* Addressing the needs
** What is at stake
   #+INCLUDE: "../../common/modules/swh-ardc.org::#compactstakes" :only-contents t :minlevel 3
   #+BEAMER: \pause
*** 
    \hfill Here we will focus on ARDC
** Where is the source code?                                    :skipinshort:
   #+INCLUDE: "../../common/modules/forges-not-archives.org::#categories" :only-contents t :minlevel 3
** How (not) to preserve and share research software
   #+INCLUDE: "../../common/modules/forges-not-archives.org::#oldapproaches" :only-contents t :minlevel 3
** Forges are /not/ archives!
   #+INCLUDE: "../../common/modules/forges-not-archives.org::#evidence" :only-contents t :minlevel 3
   #+BEAMER:\pause
*** 
    \hfill We need a universal archive of software source code: \pause now we have one!
* Meet Software Heritage: a radically different approach
** Software Heritage in a nutshell \hfill www.softwareheritage.org
#+BEAMER: \transdissolve
#+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :only-contents t :minlevel 3
** An international, non profit initiative\hfill built for the long term
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .5
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}{\includegraphics[width=\extblockscale{.4\linewidth}]{unesco_logo_en_285}}\end{center}
   #+LATEX: \vspace{-0.8cm}
   #+LATEX: \begin{center}\vskip 1em \includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
    #+latex: \small And many more ...\\
    #+latex:\mbox{}~~~~~~~\tiny\url{www.softwareheritage.org/support/testimonials}
#+BEAMER: \pause
*** Donors, members, sponsors                                       :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.4\linewidth}]{inria-logo-new}\end{center}
   #+LATEX: \begin{center}
   # #+LATEX: \includegraphics[width=\extblockscale{.2\linewidth}]{sponsors-levels.pdf}
   #+LATEX: \colorbox{white}{\includegraphics[width=\extblockscale{1.4\linewidth}]{sponsors.pdf}}
   #+LATEX: \end{center}
#   - sponsoring / partnership :: \hfill \url{sponsorship.softwareheritage.org}
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Research collaboration                              :B_picblock:noexport:
  :PROPERTIES:
  :BEAMER_COL: .5
  :BEAMER_env: picblock
  :BEAMER_OPT: pic=Qwant_Logo, leftpic=true
  :END:
    source code search engine
*** See more                                                       :noexport:
    \hfill\tiny\url{http:://www.softwareheritage.org/support/testimonials}
*** Global network                                      :B_picblock:noexport:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=fossid, leftpic=true, width=.3\linewidth
    :END:
     - first *independent mirror*
     - increased reliability
** The largest software archive, a shared infrastructure
#+BEAMER: \transdissolve
#+INCLUDE: "../../common/modules/principles-compact.org::#sharedinfra" :only-contents t :minlevel 3
** Address common Open Science and Open Source needs: archival
   #+INCLUDE: "../../common/modules/under-the-hood-pictures.org::#automation" :only-contents t :minlevel 3
** Address common Open Science and Open Source needs: reference
   #+INCLUDE: "../../common/modules/swh-ardc.org::#swh-r-common" :only-contents t :minlevel 3
* Demo time!                                                  
** A walkthrough
   #+INCLUDE: "../../common/modules/swh-ardc.org::#demoswhhal2" :only-contents t :minlevel 3
* Collaboration with UniPi: software source code as Heritage
** Calling for preservation: UNESCO
   #+INCLUDE: "../../common/modules/policyactions.org::#pariscall2019heritage" :only-contents t :minlevel 3
** Calling for preservation: Donald Knuth and Len Shustek
*** Communications of the ACM, February 2021                     :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=KnuthHistory.jpg, leftpic=true, width=.3\textwidth
    :END:
    /"Telling historical stories is the best way to teach. It's much easier to understand something if you know the threads it is connected to."/
    \mbox{}\\
    \mbox{}\\
    \mbox{}\hfill /Let's Not Dumb Down the History of Computer Science/\\
    \mbox{}\hfill Donald E. Knuth, Len Shustek\\
    \mbox{}\hfill https://doi.org/10.1145/3442377
#+BEAMER: \pause
*** A unique opportunity
    most of the creators are still here: we can talk to them!\\
    \hfill but the clock is ticking...
#    - Software Heritage provides a key infrastructure for software historians
** /\href{https://unesdoc.unesco.org/ark:/48223/pf0000371017}{SWHAP}/ process, with UNESCO and University of Pisa \hfill November 2019
  #+INCLUDE: "../../common/modules/swh-acquisition-process.org::#swhap" :only-contents t :minlevel 3
** Building the Software Stories \hfill November 2021
  #+INCLUDE: "../../common/modules/swh-acquisition-process.org::#stories" :only-contents t :minlevel 3
* Collaboration with UniPi: software source code as Knowledge
** Hold your breath!
   - large scale /compression/ of the archive
   - large scale, efficient, /search/ in the archive
   - ... and much more
* Software Heritage, cont'd   
** Software Heritage: key infrastructure for Open Science       :skipinshort:
*** [[https://data.europa.eu/doi/10.2777/28598][EOSC SIRS report]]: Software Source Code and Open Science, 2020 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=eosc-sirs-architecture-swh.png, leftpic=true, width=.5\textwidth
    :END:
    Connect scholarly ecosystem with the whole software ecosystem\\
    See e.g. [[https://code.gouv.fr/#/repos][the French public administration open source catalog]] 
*** Ongoing work: [[https://faircore4eosc.eu/][FAIRCORE4EOSC]]
    A full workpackage:
     - connectors with InvenioRDM, episcience, Dagstuhl, swMath, etc.
     - Software Heritage mirror for the EOSC
     - standardisation of CodeMeta and SWHID
** Software Heritage: a shared open infrastructure
*** Software Heritage offers                                        :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .58
    :END:
    - *archival* of all public *source code*
    - *reference* of all public *source code*
    - *sharing* cost with other partners
    - *standards based* approach
#+BEAMER: \pause
*** Software Heritage is                                                             :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .4
    :END:
    - *vendor neutral*
    - *open source*
    - *worldwide*, *long term*
    - *born and based in the EU*
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    #+BEAMER: \pause
*** Getting onboard: join the Deposit Interest Group               :noexport:
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .4
     :END:
    Four levels:
      - Strategic
      - Core
      - Solutions
      - Basic
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .5
     :END:
    - Flat membership fee
    - Participation in advisory boards (next is February 1st 2024)
    - Access to working groups
** A growing and active community
***                                                                :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .5
    :END:
**** [[https://softwareheritage.org/people/][Team]]
     :PROPERTIES:
     :BEAMER_env: block
     :END:
     #+ATTR_LATEX: :width \linewidth
     file:2023-team-large.jpg
     #+BEAMER: \pause
****                                                        :B_ignoreheading:
     :PROPERTIES:
     :BEAMER_env: ignoreheading
     :END:
     #+BEAMER: \vspace{-.3em}     
**** [[https://softwareheritage.org/ambassadors/][Ambassadors]]
     :PROPERTIES:
     :BEAMER_env: block
     :END:
     #+ATTR_LATEX: :width .7\linewidth
     file:ambassadors-2023.jpg
     #+BEAMER: \pause
***                                                                :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_COL: .5
    :END:
**** [[https://web.libera.chat/?nick=swhguest?#swh-devel][Contributors to the platform]]
     :PROPERTIES:
     :BEAMER_env: block
     :END:
     #+ATTR_LATEX: :width \linewidth
     file:swh-team-chat-snapshot.png
     #+BEAMER: \pause
**** Awards
     :PROPERTIES:
     :BEAMER_env: block
     :END:
     #+ATTR_LATEX: :width \linewidth
     file:swh-awards.png
** Report and videos
*** Annual report                                            :B_column:BMCOL:
    :PROPERTIES:
    :BEAMER_col: .4
    :BEAMER_env: block
    :END:
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_COL: .8
     :BEAMER_env: column
     :END:
    #+ATTR_LATEX: :width .8\linewidth
    file:2022-report-swh.png
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_COL: .3
     :BEAMER_env: column
     :END:
     \qrcode[height=2em]{https://www.softwareheritage.org/wp-content/uploads/2023/02/SoftwareHeritageReport_2022.pdf}\hfill\mbox{}
     #+BEAMER: \pause
***                                                           :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: .4
    :BEAMER_env: column
    :END:
**** 5 years in 5 minutes\hfill      [[https://www.youtube.com/watch?v=8RdA5TxOAGo][Link]]                           :B_block:
     :PROPERTIES:
     :BEAMER_env: block
     :END:
*****                                                              :B_column:
      :PROPERTIES:
      :BEAMER_COL: .8
      :BEAMER_env: column
      :END:
     #+ATTR_LATEX: :width .8\linewidth
     [[https://www.youtube.com/watch?v=Ez4xKTKJO2o][file:5y5m-video.png]]
*****                                                              :B_column:
      :PROPERTIES:
      :BEAMER_env: column
      :BEAMER_COL: .2
      :END:
     \qrcode[height=2em]{https://www.youtube.com/watch?v=8RdA5TxOAGo}
    #+BEAMER: \pause
**** Evolution of our codebase\hfill [[https://youtu.be/UzvPu4WfbLQ][Link]]                           :B_block:
     :PROPERTIES:
     :BEAMER_env: block
     :END:
    #+ATTR_LATEX: :width .8\linewidth
     [[https://youtu.be/UzvPu4WfbLQ][file:swh-evolution-video.png]]
   
