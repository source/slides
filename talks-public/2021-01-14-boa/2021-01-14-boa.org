#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage and Boa
#+BEAMER_HEADER: \date[Boa workshop]{Envisioning Boa 2.0 workshop\\online conference}
#+AUTHOR: Stefano Zacchiroli
#+DATE: 14 January 2021
#+EMAIL: zack@upsilon.cc

#+INCLUDE: "../../common/modules/prelude.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+BEAMER_HEADER: \titlegraphic{}
#+BEAMER_HEADER: \setbeamertemplate{background}{\pgfuseimage{bgd-world}}
#+BEAMER_HEADER: \institute[UParis \& Inria]{Université de Paris \& Inria --- {\tt zack@upsilon.cc, @zacchiro}}
#+BEAMER_HEADER: \author{Stefano Zacchiroli}
#+BEAMER_HEADER: \title{Software Heritage and Boa}

* Software Heritage
** Software Heritage in a nutshell \hfill www.softwareheritage.org
   #+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :only-contents t :minlevel 3
** An international, non profit initiative\hfill built for the long term
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .5
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}{\includegraphics[width=\extblockscale{.4\linewidth}]{unesco_logo_en_285}}\end{center}
   #+LATEX: \vspace{-0.8cm}
   #+LATEX: \begin{center}\vskip 1em \includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
    #+latex: \small And many more ...\\
    #+latex:\mbox{}~~~~~~~\tiny\url{www.softwareheritage.org/support/testimonials}
# #+BEAMER: \pause
*** Donors, members, sponsors                                       :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.4\linewidth}]{inria-logo-new}\end{center}
   #+LATEX: \begin{center}
   # #+LATEX: \includegraphics[width=\extblockscale{.2\linewidth}]{sponsors-levels.pdf}
   #+LATEX: \colorbox{white}{\includegraphics[width=\extblockscale{1.4\linewidth}]{sponsors.pdf}}
   #+LATEX: \end{center}
#   - sponsoring / partnership :: \hfill \url{sponsorship.softwareheritage.org}
** Data model
   #+BEAMER: \centering \includegraphics[width=\textwidth]{swh-data-model-h}
*** 
    A *global graph* linking together fully *deduplicated* source code artifact
    (files, commits, directories, releases, etc.) to the places that distribute
    them (e.g., Git repositories), providing a *unified view* on the entire
    */Software Commons/*.
** Automation, and storage
   #+BEAMER: \begin{center}
   #+BEAMER:   \mode<beamer>{\only<1>{\includegraphics[width=\extblockscale{1.1\textwidth}]{swh-dataflow-merkle-listers.pdf}}}
   #+BEAMER:   \only<2-3>{\includegraphics[width=\extblockscale{1.1\textwidth}]{swh-dataflow-merkle.pdf}}
   #+BEAMER: \end{center}
   #+BEAMER: \pause
   #+BEAMER: \pause
   Full development history *permanently archived* in a *uniform data model*.
** The largest free/open source software archive \hfill\small archive.softwareheritage.org
   #+INCLUDE: "../../common/modules/status-extended.org::#archive" :only-contents t :minlevel 3
** Selected research highlights
  #+BEGIN_EXPORT latex
  \begin{thebibliography}{Foo Bar, 1969}
  \small

  \bibitem{Abramatic2018} Jean-François Abramatic, Roberto Di Cosmo, Stefano Zacchiroli
  \newblock Building the Universal Archive of Source Code
  \newblock Communication of the ACM, October 2018

  \bibitem{Pietri2019} Antoine Pietri, Diomidis Spinellis, Stefano Zacchiroli
  \newblock The Software Heritage Graph Dataset: Public software development under one roof
  \newblock MSR 2019: 16th Intl. Conf. on Mining Software Repositories. IEEE

  \bibitem{Boldi2020} Paolo Boldi, Antoine Pietri, Sebastiano Vigna, Stefano Zacchiroli
  \newblock Ultra-Large-Scale Repository Analysis via Graph Compression
  \newblock SANER 2020, 27th Intl. Conf. on Software Analysis, Evolution and Reengineering. IEEE

  \bibitem{Pietri2020a} Antoine Pietri, Guillaume Rousseau, Stefano Zacchiroli
  \newblock Forking Without Clicking: on How to Identify Software Repository Forks
  \newblock MSR 2020: 17th Intl. Conf. on Mining Software Repositories. IEEE

  \bibitem{Rousseau2020} Roberto Di Cosmo, Guillaume Rousseau, Stefano Zacchiroli
  \newblock Software Provenance Tracking at the Scale of Public Source Code
  \newblock Empirical Software Engineering, 2020

  \end{thebibliography}
  #+END_EXPORT
* Collaboration
** Working together
   Analyses of the Software Heritage archive are still performed in ad-hoc ways
   that require quite some knowledge of the archive data model and caveats.
*** A Boa-like runtime for Software Heritage
    - a *filtering language* to determine which artifacts and parts of the
      archive to analyze
    - an *implementation language* to describe experiments on artifacts
      (general purpose with a dedicated library, DSL, or otherwise)
    - a *runtime* for executing experiments, exploiting DAG node sharing to
      avoid redoing unneeded expensive computations
    - *infrastructure connectors* that allow to run experiments
      (locally/cluster/cloud)
*** Sharing archive access
    - we are archiving everything /anyway/, for digital preservation purposes
    - an archive mirror could be leveraged by Boa as corpus for researchers
* Conclusion
** Wrapping up
*** 
    - *Software Heritage archives* source code artifacts for posterity and it
      is the most comprehensive publicly accessible archive of its kind
    - *Boa* harvests source code artifacts and provides an *environment* that
      enables researchers to run experiments on them
    - Software Heritage provides valuable research datasets, but its
      researcher-facing part is not as developed as Boa's
    - there is mutual benefit in *sharing access* to source code artifacts and
      *porting Boa's runtime* to the Software Heritage archive and data model
*** Learn more
    #+BEAMER: \center \Large
    [[https://www.softwareheritage.org][www.softwareheritage.org]]
*** Contacts
    [[https://upsilon.cc/~zack/][Stefano Zacchiroli]] \hfill/\hfill [[mailto:zack@upsilon.cc][zack@upsilon.cc]] \hfill/\hfill [[https://twitter.com/zacchiro][@zacchiro]]
    \hfill/\hfill [[https://mastodon.xyz/@zacchiro][@zacchiro@mastodon.xyz]]
* Appendix                                                       :B_appendix:
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
