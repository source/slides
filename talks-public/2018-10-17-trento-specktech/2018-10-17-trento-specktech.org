#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage
#+SUBTITLE: Archiving the Free Software Commons for Fun & Profit
#+BEAMER_HEADER: \date[17/10/2018, PyConFr]{17 Oct 2018\\Speck \& Teck - Trento, Italy}
#+DATE: 17 October 2018

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+BEAMER_HEADER: \institute[Software Heritage]{Software Heritage --- {\tt zack@upsilon.cc, @zacchiro}}
#+BEAMER_HEADER: \author{Stefano Zacchiroli}

#+LATEX_HEADER_EXTRA: \usepackage{bbding}
#+LATEX_HEADER_EXTRA: \DeclareUnicodeCharacter{66D}{\FiveStar}
#+LATEX_HEADER_EXTRA: \usepackage{tikz}
#+LATEX_HEADER_EXTRA: \usetikzlibrary{arrows,shapes}
#+LATEX_HEADER_EXTRA: \definecolor{swh-orange}{RGB}{254,205,27}
#+LATEX_HEADER_EXTRA: \definecolor{swh-red}{RGB}{226,0,38}
#+LATEX_HEADER_EXTRA: \definecolor{swh-green}{RGB}{77,181,174}

# Syntax highlighting setup

#+LATEX_HEADER_EXTRA: \usepackage{minted}
#+LaTeX_HEADER_EXTRA: \usemintedstyle{tango}
#+LaTeX_HEADER_EXTRA: \newminted{python}{fontsize=\scriptsize}
#+LaTeX_HEADER_EXTRA: \newminted{html}{fontsize=\scriptsize}

#+name: setup-minted
#+begin_src emacs-lisp :exports results :results silent
   (setq org-latex-listings 'minted)
   (setq org-latex-minted-options
         '(("fontsize" "\\scriptsize")
           ("linenos" "")))
   (setq org-latex-to-pdf-process
         '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
           "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
           "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
#+end_src

# End syntax highlighting setup

* The Software Commons
** (Free) Software is everywhere
   #+latex: \begin{center}
   #+ATTR_LATEX: :width .75\linewidth
   file:software-center.pdf
   #+latex: \end{center}
   #+INCLUDE: "../../common/modules/source-code-different-short.org::#softwareisdifferent" :minlevel 2

** Our Software Commons
   #+INCLUDE: "../../common/modules/foss-commons.org::#commonsdef" :only-contents t
   #+BEAMER: \pause
*** Source code is /a precious part/ of our commons
     \hfill are we taking care of it?
  #+INCLUDE: "../../common/modules/swh-motivations-foss.org::#main" :only-contents t :minlevel 2

* Software Heritage
 #+INCLUDE: "../../common/modules/swh-overview-sourcecode.org::#mission" :minlevel 2
** Core principles
   #+latex: \begin{center}
   #+ATTR_LATEX: :width .9\linewidth
   file:SWH-as-foundation-slim.png
   #+latex: \end{center}
   #+BEAMER: \pause
*** Open approach 					      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
    - 100% Free Software
    - transparency
*** In for the long haul 				      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.4
    :BEAMER_env: block
    :END:
    - replication
    - non profit
 #+INCLUDE: "../../common/modules/status-extended.org::#archivinggoals" :minlevel 2
 #+INCLUDE: "../../common/modules/status-extended.org::#architecture" :minlevel 2 :only-contents t
 #+INCLUDE: "../../common/modules/status-extended.org::#dagdetail" :minlevel 2 :only-contents t
 #+INCLUDE: "../../common/modules/status-extended.org::#archive" :minlevel 2

# * Accessing the archive
 # #+INCLUDE: "../../common/modules/status-extended.org::#api" :only-contents t
 #+INCLUDE: "../../common/modules/status-extended.org::#apiintro" :minlevel 2
 #+INCLUDE: "../../common/modules/vault.org::#overview" :minlevel 2
 #+INCLUDE: "../../common/modules/webui.org::#intro" :minlevel 2

* Case study: archiving PyPI
** Latest addition to the archive
   https://www.softwareheritage.org/2018/10/10/pypi-available-on-software-heritage/
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .4
    :END:
   #+BEAMER: \includegraphics[width=\linewidth]{pypi-blog-post}
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .4
    :END:
    - reference package repositories for the Python community
    - volunteer run
    - 150 K packages
    - 1.1 M releases
    - 1.5 M files

 #+INCLUDE: "../../common/modules/pypi-loader.org::#main" :only-contents t :minlevel 2
* Getting involved
 #+INCLUDE: "../../common/modules/status-extended.org::#features" :minlevel 2
** You can help!
   #+BEAMER: \vspace{-3mm}
*** Coding
    | ٭٭  | Web UI improvements                             |
    | ٭٭٭ | loaders for unsupported VCS/package formats     |
    | ٭٭٭ | listers for unsupported forges/package managers |
    #+BEAMER: \vspace{-2mm} \footnotesize \centering
    \url{https://forge.softwareheritage.org} \\
    \url{https://docs.softwareheritage.org/devel}
    #+BEAMER: \pause
*** Community
    | ٭٭٭ | spread the world, help us with sustainability |
    | ٭٭  | document endangered source code               |
    #+BEAMER: \vspace{-2mm} \footnotesize \centering
    \url{https://wiki.softwareheritage.org/Suggestion_box}
    #+BEAMER: \pause
*** Join us
    #+BEAMER: \footnotesize \centering
    - \url{https://www.softwareheritage.org/jobs} --- *job openings*
    - \url{https://wiki.softwareheritage.org/Internship} --- *internships*
** Conclusion
*** Software Heritage is
    - a reference archive of *all Free Software* ever written
    - an international, open, nonprofit, *mutualized infrastructure*
    - *now accessible* to developers, users, vendors
    - at the service of our community, *at the service of society*
*** Come in, we're open!
    \url{www.softwareheritage.org} --- general information \\
    \url{wiki.softwareheritage.org} --- internships, leads \\
    \url{forge.softwareheritage.org} --- our own code
