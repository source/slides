\documentclass[a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{qrcode}
\usepackage{graphicx}
\usepackage{geometry}
\usepackage{float}
\usepackage{layout}
\usepackage[absolute,overlay]{textpos}

\graphicspath{%
{pics/}{images/}{./}%
}

\geometry{margin=0.7in}
\marginparwidth 0pt
\marginparsep 0pt
\textwidth 520pt

\begin{document}
\begin{textblock*}{4cm}(1cm,1cm) % {block width} (coords) 
    \includegraphics[width=4cm]{software-heritage-logo-title.512px}
\end{textblock*}

\begin{textblock*}{4cm}(\dimexpr\paperwidth-5cm\relax,1cm) 
    \includegraphics[width=4cm]{inria-logo-new}
\end{textblock*}

\begin{textblock*}{3cm}(1cm,2.5cm)
    \includegraphics[width=3cm]{2022-report-swh}
\end{textblock*}

\begin{textblock*}{2cm}(\dimexpr\paperwidth-4cm\relax,3.5cm) 
    \qrcode{https://www.softwareheritage.org/wp-content/uploads/2023/02/SoftwareHeritageReport_2022.pdf}
\end{textblock*}


\title{Software Heritage for Open Science}
\author{Roberto Di Cosmo\\
  director, Software Heritage\\
  \href{https://dicosmo.org}{https://dicosmo.org}\hspace{3em}\href{mailto:roberto@dicosmo.org}{roberto@dicosmo.org}}
\date{October 2023}
\maketitle
% \layout
\begin{minipage}{0.9\textwidth}
  Software is a cornerstone of Open Science. Software Heritage has worked since 2016 to provide the reference infrastructure for research source codes, at all levels. Here we present a selection of actions and their impact at the French, European, and international levels.
\end{minipage}

\section{France}
\subsection{Integration with HAL (2016-\ldots)}
    \begin{minipage}{0.75\textwidth}
        Integration of Software Heritage with HAL to enable the \href{https://doc.archives-ouvertes.fr/deposer/deposer-le-code-source/}{deposit of research source codes}. Collaboration began with CCSD in 2016, and has intensified since, with a national deployment based on the \href{https://swhid.org}{Software Hash Identifier (SWHID)}, the CodeMeta metadata format, and the \href{https://ctan.org/pkg/biblatex-software}{BibLaTeX \texttt{biblatex-software}} style.
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://doc.archives-ouvertes.fr/deposer/deposer-le-code-source/}
    \end{minipage}

\subsection{National Plan for Open Science (2021-2024)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=.8\textwidth]{PNSO2-cover}
    \end{minipage}
    \hfill
    \begin{minipage}{0.5\textwidth}
        Recognition of archiving and referencing of research source codes as a key component of the Open Science software pillar. The \href{https://www.enseignementsup-recherche.gouv.fr/fr/le-plan-national-pour-la-science-ouverte-2021-2024-vers-une-generalisation-de-la-science-ouverte-en-48525}{national plan for Open Science} relies on Software Heritage to meet these needs at the national level in France.
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://www.enseignementsup-recherche.gouv.fr/fr/le-plan-national-pour-la-science-ouverte-2021-2024-vers-une-generalisation-de-la-science-ouverte-en-48525}
    \end{minipage}

\subsection{ANR Recommendation (2023-\ldots)}
    \begin{minipage}{0.75\textwidth}
      The National Agency for Research (ANR) now includes a recommendation for archiving source codes developed with its funding, using Software Heritage (see \href{https://anr.fr/fileadmin/aap/2023/aapg-2023-V2.0_MAJ20220921.pdf}{page 17 of the call for projects guide}).
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://anr.fr/fileadmin/aap/2023/aapg-2023-V2.0_MAJ20220921.pdf}
    \end{minipage}

\subsection{National Strategy for Research Infrastructures (2021-\ldots)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=.8\textwidth]{french-ri-roadmap-2021}
    \end{minipage}
    \hfill
    \begin{minipage}{0.5\textwidth}
      Inclusion of Software Heritage in the \href{https://www.enseignementsup-recherche.gouv.fr/fr/la-strategie-nationale-des-infrastructures-de-recherche-46112}{national roadmap for research infrastructures}, published in March 2022. This is the first infrastructure serving software to appear in this kind of document: software is no longer just a tool serving research infrastructures.
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://www.enseignementsup-recherche.gouv.fr/fr/la-strategie-nationale-des-infrastructures-de-recherche-46112}
    \end{minipage}

\clearpage
\section{European Level}

\subsection{EOSC SIRS Report (2020)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=.7\textwidth]{EOSC-SIRS-report}
    \end{minipage}
    \hfill
    \begin{minipage}{0.5\textwidth}
      Published in 2020, the
      \href{https://data.europa.eu/doi/10.2777/28598}{EOSC SIRS report}
      (Scholarly Infrastructures for Research Software) identifies Software Heritage as the shared infrastructure on which institutional repositories, publishers, and aggregators should rely for the archiving and referencing of research software.
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://data.europa.eu/doi/10.2777/28598}
    \end{minipage}

\subsection{FAIRCORE4EOSC Project (2022-2025)}
    \begin{minipage}{0.25\textwidth}
        \includegraphics[width=\textwidth]{fc4e-rsac}
    \end{minipage}
    \hfill
    \begin{minipage}{0.5\textwidth}
      The European FAIRCORE4EOSC project, funded up to 9 million euros, includes a workpackage specifically dedicated to implementing the recommendations of the EOSC SIR report, through the
      \href{https://faircore4eosc.eu/eosc-core-components/eosc-research-software-apis-and-connectors-rsac}{development of specific connectors and APIs}.
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://faircore4eosc.eu/eosc-core-components/eosc-research-software-apis-and-connectors-rsac}
    \end{minipage}

\subsection{Mirrors (2024-\ldots)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=\textwidth]{fc4e-swhm}
    \end{minipage}
    \hfill
    \begin{minipage}{0.55\textwidth}
      Within the European FAIRCORE4EOSC project, work is underway to build a complete mirror of the Software Heritage archive for open science in Europe. Once in place, the mirror will be maintained by GRNET, the infrastructure centralizing network and computing management for the entire Greek academic ecosystem.
        \href{https://faircore4eosc.eu/eosc-core-components/eosc-software-heritage-mirror}{Link to the mirrors}
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://faircore4eosc.eu/eosc-core-components/eosc-software-heritage-mirror}
    \end{minipage}

\section{International Level}

\subsection{Paris Call for Software Source Code (2018, UNESCO)}
    \begin{minipage}{0.15\textwidth}
        \includegraphics[width=\textwidth]{paris_call_ssc_cover}
    \end{minipage}
    \hfill
    \begin{minipage}{0.55\textwidth}
      Resulting from a group of international experts convened by UNESCO and Inria, it contains this passage: "promote software development as a \emph{valuable research activity}, and research software as a key enabler for Open Science/Open Research, [\ldots{}] recognising in the careers of academics their contributions to \emph{high quality software development}, in all their forms".
    \end{minipage}
        \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://en.unesco.org/foss/paris-call-software-source-code}
    \end{minipage}

\subsection{Normalization of SWHID Identifiers (2023-\ldots)}
    \begin{minipage}{0.3\textwidth}
        \includegraphics[width=\textwidth]{SWHID-v1.4_3}
    \end{minipage}
    \hfill
    \begin{minipage}{0.45\textwidth}
      Software Heritage uses an intrinsic cryptographic identifier, the SWHID, for the 30+ billion archived artifacts. This identifier is now called "Software Hash Identifier" and is the subject of a \href{https://swhid.org}{public specification that will lead to an ISO normalization submission}.
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://swhid.org}
    \end{minipage}

\subsection{Contribution to the \emph{Codemeta} Software Metadata Schema (2020-\ldots)}
    \begin{minipage}{0.2\textwidth}
        \includegraphics[width=\textwidth]{codemetaproject}
    \end{minipage}
    \hfill
    \begin{minipage}{0.55\textwidth}
        Software Heritage collects metadata on archived software. It has identified \href{https://github.com/codemeta/codemeta}{CodeMeta} as the reference metadata schema, and the \texttt{codemeta.json} file as the pivot format, machine-readable. Software Heritage plays a key role in the evolution and maintenance of this format, aiming for its integration into \texttt{schema.org}.
    \end{minipage}
    \hfill
    \begin{minipage}{0.2\textwidth}
        \qrcode{https://github.com/codemeta/codemeta}
    \end{minipage}
\end{document}
