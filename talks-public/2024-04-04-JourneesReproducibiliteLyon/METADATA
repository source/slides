Titre: "Pérenniser la Recherche avec Software Heritage : Archivage,
Référencement, Description et Citation des Codes Sources"

Résumé:

Le logiciel joue un rôle majeur dans tous les domaines scientifiques, et la
connaissance technique et scientifique se retrouve dans leurs codes
sources. Garantir la disponibilité à long terme de ces codes sources, ainsi que
référencer la version exacte utilisée, est un enjeu essentiel pour la
transparence et la reproductibilité des résultats de la recherche.  Nous allons
revoir l'état de l'art, et explorer comment répondre facilement à ces besoins en
s'appuyant sur Software Heritage, une infrastructure sans but lucratif en
partenariat avec l'UNESCO qui archive déjà plus de 17 milliards de fichiers
provenant de plus de 270 millions de projets publics.

Nous aborderons aussi la question de la description et la citation des codes
sources, soulignant leur importance pour une recherche collaborative et
efficace. Des exemples concrets illustreront l'exposé, qui s'adresse à tous les
chercheurs et ingénieurs désireux d'améliorer la durabilité et l'efficacité de
leurs travaux de recherche.
