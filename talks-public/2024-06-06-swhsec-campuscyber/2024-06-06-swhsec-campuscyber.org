#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Software Heritage
#+SUBTITLE: Large-Scale Research on Public Code
#+BEAMER_HEADER: \date[2024-06-06, CampusCyber]{6 Jun 2024 --- CampusCyber, Paris}
#+AUTHOR: Stefano Zacchiroli
#+DATE: 6 June 2024
#+EMAIL: stefano.zacchiroli@telecom-paris.fr

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+BEAMER_HEADER: \institute[Télécom Paris]{Télécom Paris, Institut Polytechnique de Paris\\ {\tt stefano.zacchiroli@telecom-paris.fr}}
#+BEAMER_HEADER: \author{Stefano Zacchiroli}

* Preface                                                   :B_ignoreheading:
** About the speaker
  :PROPERTIES:
  :CUSTOM_ID: bio
  :END:
*** 
    - Professor of Computer Science, Télécom Paris, Institut Polytechnique de
      Paris
    - Free/Open Source Software activist (20+ years)
    - Debian Developer & Former 3x Debian Project Leader
    - Former Open Source Initiative (OSI) director
    - Software Heritage co-founder & CTO
* Software Heritage
#+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :minlevel 2
** Status                                                   :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:
# #+INCLUDE: "../../common/modules/status-extended.org::#archivinggoals" :minlevel 2
#+INCLUDE: "../../common/modules/status-extended.org::#architecture" :minlevel 2 :only-contents t
# #+INCLUDE: "../../common/modules/status-extended.org::#merkletree" :minlevel 2
#+INCLUDE: "../../common/modules/data-model.org::#merklestruct" :minlevel 2
#+INCLUDE: "../../common/modules/status-extended.org::#dagdetailsmall" :minlevel 2 :only-contents t
#+INCLUDE: "../../common/modules/swhid.org::#oneslide" :minlevel 2
#+INCLUDE: "../../common/modules/status-extended.org::#archive" :minlevel 2
** A quick tour
   - Browse [[https://archive.softwareheritage.org][the archive]]
   - [[https://save.softwareheritage.org][Trigger archival]] of your preferred software in a breeze
   # - Get and use SWHIDs ([[https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html][full specification available online]])
   - The [[https://www.softwareheritage.org/2019/07/20/archiving-and-referencing-the-apollo-source-code/][Apollo 11 AGC source code example]]
   - Cite software [[https://www.softwareheritage.org/2020/05/26/citing-software-with-style/][with the biblatex-software style]] from CTAN
   - Example use in a research article: compare Fig. 1 and conclusions 
      - in [[http://www.dicosmo.org/Articles/2012-DaneluttoDiCosmo-Pcs.pdf][the 2012 version]]
      - in [[https://www.dicosmo.org/share/parmap_swh.pdf][the updated version]] using SWHIDs and Software Heritage
#   - Example use in a research article: extensive use of SWHIDs in [[https://www.dicosmo.org/Articles/2020-ReScienceC.pdf][a replication experiment]]
   - Example in a journal: [[http://www.ipol.im/pub/art/2020/300/][an article from IPOL]]
   - [[https://doc.archives-ouvertes.fr/en/deposit/deposit-software-source-code/][Curated deposit in SWH via HAL]], see for example: 
      [[https://hal.archives-ouvertes.fr/hal-02130801][LinBox]], [[https://hal.archives-ouvertes.fr/hal-01897934][SLALOM]], [[https://hal.archives-ouvertes.fr/hal-02130729][Givaro]], [[https://hal.archives-ouvertes.fr/hal-02137040][NS2DDV]], [[https://hal.archives-ouvertes.fr/lirmm-02136558][SumGra]], [[https://hal.archives-ouvertes.fr/hal-02155786][Coq proof]], ...
   - Rescue landmark legacy software, see the [[https://www.softwareheritage.org/swhap/][SWHAP process with UNESCO]]
* Research highlights
** Graph dataset
#+INCLUDE: "../../common/modules/dataset.org::#graphdataset" :only-contents t
** Graph dataset --- example
#+INCLUDE: "../../common/modules/dataset.org::#graphquery1" :only-contents t
** Graph dataset --- example
#+INCLUDE: "../../common/modules/dataset.org::#graphquery2" :only-contents t
** License dataset
#+INCLUDE: "../../common/modules/dataset.org::#licensedataset" :only-contents t
** The Software Heritage Filesystem (SwhFS)
#+INCLUDE: "../../common/modules/swh-fuse.org::#oneslide" :only-contents t
** The Software Heritage Filesystem (SwhFS) --- example           :noexport:
#+INCLUDE: "../../common/modules/swh-fuse.org::#examplemini" :only-contents t
** The Software Heritage Filesystem (SwhFS) --- example (cont.)   :noexport:
#+INCLUDE: "../../common/modules/swh-fuse.org::#tutorial4" :only-contents t
** Graph compression
#+INCLUDE: "../../common/modules/graph-compression.org::#oneslide" :only-contents t
# #+INCLUDE: "../../common/modules/graph-compression.org::#background1" :minlevel 2
# #+INCLUDE: "../../common/modules/graph-compression.org::#background2" :minlevel 2
# #+INCLUDE: "../../common/modules/graph-compression.org::#pipeline" :minlevel 2
** Software provenance and evolution                        :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:
#+INCLUDE: "../../common/modules/ese-research.org::#provenance" :minlevel 2 :only-contents t
** Diversity, equity, and inclusion                         :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:
#+INCLUDE: "../../common/modules/ese-research.org::#diversity" :minlevel 2 :only-contents t
** ... and more                                            :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:
#+INCLUDE: "../../common/modules/ese-research.org::#leads" :minlevel 2
* Conclusion
** Conclusion
*** 
    - Software Heritage archives public code and its history as a huge Merkle DAG
    - Analyzing it at scale (35/500 B nodes/edges) is a significant big data undertaking
    - Gold mine of research leads in and around empirical software engineering
*** Research on Software Heritage
[[https://www.softwareheritage.org/publications/][www.softwareheritage.org/publications]]
*** Questions?
    - Ask me!
*** Contact
    [[https://upsilon.cc/zack][Stefano Zacchiroli]] / [[mailto:stefano.zacchiroli@telecom-paris.fr][stefano.zacchiroli@telecom-paris.fr]] / [[https://mastodon.xyz/@zacchiro][@zacchiro@mastodon.xyz]]
* Appendix                                                       :B_appendix:
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
** 
   \vfill
   \centerline{\Huge Appendix}
   \vfill

#+INCLUDE: "../../common/modules/swh-vuln.org::#swh-vuln" :minlevel 2 :only-contents t

** An international, non profit initiative\hfill built for the long term
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .5
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}{\includegraphics[width=\extblockscale{.4\linewidth}]{unesco_logo_en_285}}\end{center}
   #+LATEX: \vspace{-0.8cm}
   #+LATEX: \begin{center}\vskip 1em \includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
   #+latex:\begin{center}\tiny\url{www.softwareheritage.org/support/testimonials}\end{center}
  # #+BEAMER: \pause
*** Donors, members, sponsors                                       :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.4\linewidth}]{inria-logo-new}\end{center}
   #+LATEX: \vspace{-1cm}
   #+LATEX: \begin{center}
   #+LATEX: \colorbox{white}{\includegraphics[width=\extblockscale{1.4\linewidth}]{sponsors.pdf}}
   #+LATEX: \end{center}
   #+latex:\begin{center}\tiny\url{www.softwareheritage.org/support/sponsors}\end{center}
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Research collaboration                              :B_picblock:noexport:
  :PROPERTIES:
  :BEAMER_COL: .5
  :BEAMER_env: picblock
  :BEAMER_OPT: pic=Qwant_Logo, leftpic=true
  :END:
    source code search engine
*** See more                                                       :noexport:
    \hfill\tiny\url{http:://www.softwareheritage.org/support/testimonials}
*** Global network                                      :B_picblock:noexport:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=fossid, leftpic=true, width=.3\linewidth
    :END:
     - first *independent mirror*
     - increased reliability
