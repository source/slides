#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Building Blocks for a Safer Open Source Supply Chain
#+SUBTITLE: Reproducible Builds and Software Heritage
#+AUTHOR: Stefano Zacchiroli
#+EMAIL: zack@upsilon.cc @zacchiro

#+BEAMER_HEADER: \date[29 May 2024]{29 May 2024\\INFORTECH Day 2024, Université de Mons\\Mons, Belgium\\[-3ex]}
#+BEAMER_HEADER: \author[Stefano Zacchiroli~~~~ zack@upsilon.cc ~~~~ (CC-BY-SA 4.0)]{Stefano Zacchiroli}
#+BEAMER_HEADER: \institute[Polytech Paris]{Software Heritage\\Télécom Paris, Polytechnic Institute of Paris}

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"

* Introduction
** About the speaker
*** 
    - Professor of Computer Science, Télécom Paris, Polytechnic Institute of Paris
    - Free/Open Source Software activist (20+ years)
    - Debian Developer & Former 3x Debian Project Leader
    - Former Open Source Initiative (OSI) director
    - Software Heritage co-founder & CTO
    - Reproducible Builds board member
* Open Source Software Supply Chain --- Attacks
#+INCLUDE: "../../common/modules/reproducible-builds.org::#def-supply-chain" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#foss-workflow" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#supply-chain-attacks" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#foss-supply-chain-attacks-1" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#attack-tree-injection" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#attack-compromise-build" :minlevel 2

* Reproducible Builds
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-problem-statement" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-definition" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-approach" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-small" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-ex-paths" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-ex-fs-order" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-large" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-adversarial-rebuild" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-debian-stats" :minlevel 2
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-ecosystem" :minlevel 2
# #+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-learn-more" :minlevel 2

* Open Source Software Supply Chain --- KYSW
** Open Source is growing...                                      :noexport:
  :PROPERTIES:
  :CUSTOM_ID: andreesen
  :END:
*** Software is eating the world :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    file:2011-Andreesen-software-eating-the-world.png
    /Software companies outperform/\\
    \hfill /or buy out traditional companies/\\
    \vspace{.5em}
    \hfill /Marc Andreesen, 2011/
# #+BEAMER: \pause
*** Open Source is eating the Software World :B_block:
   :PROPERTIES:
   :CUSTOM_ID: fossandreesen
   :BEAMER_env: block
   :BEAMER_COL: .5
   :END:
   #+ATTR_LATEX: :width .7\linewidth
   file:GrowthOpenSource.png
# #+BEAMER: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Reuse is the new rule
   80% to 90% of a new application is ... just reuse! \hfill (Sonatype survey, 2017)
# ** ... concerns are growing too \hfill ... KYSW is coming!
** KYSW (Know Your SoftWare)
*** 
Like KYC in banking, KYSW is now essential all over IT...
#+BEAMER: \pause   
*** Vertical approach: secure your software                         :B_block:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=CRA-EU.png,width=.4\linewidth,leftpic=true
    :END:
    Improve security of /each component/ separately
    - By law: e.g. EU [[https://digital-strategy.ec.europa.eu/en/library/cyber-resilience-act][Cyber Resilience Act]]
    - By practice: e.g. https://best.openssf.org/
    #+BEAMER: \pause    
*** Horizontal approach: all the supply chain                    :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=executiveorder.jpg,width=.4\linewidth,leftpic=true
    :END:
    \mbox{}\\
    *Sec. 4. Enhancing Software Supply Chain Security* \\
    \hfill /ensuring and attesting, to the extent practicable, to the integrity and provenance of open source software/\\
    \mbox{}\hfill [[https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/][May 2021 POTUS Executive Order]]
** A long road ahead
*** Vertical approach                                                             :B_block:
    :PROPERTIES:
    :BEAMER_COL: .65
    :BEAMER_env: block
    :END:
    improve security of /each component/ separately
*** Horizontal approach
    :PROPERTIES:
    :BEAMER_COL: .35
    :BEAMER_env: block
    :END:
    explore /the whole supply chain/
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** A few key challenging properties
      # what code is relevant for that application?
    - findability :: needs *qualified metadata*
      # can we get this code now? tomorrow? in 20 years? from that country?\\
    - availability :: needs *an archive* and a *system of identifiers*
      # is this the /right/ code?
    - integrity :: needs *crypto*
      # where are the components coming from?\\
    - traceability :: needs *a global provenance database*
      # can we reliably rebuild everything?
    - reproducibility :: needs *groundbreaking tools*
    #+BEAMER: \pause
*** 
    We need a /global coordinated effort/...\\
    \hfill and a /common, open, shared/ infrastructure to track /all (open source) software/!
* Software Heritage
** Software Heritage, in a nutshell \hfill www.softwareheritage.org
   #+BEAMER: \transdissolve
   #+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :only-contents t :minlevel 3
** Universal software archive, principled \hfill \url{http://bit.ly/swhpaper} :noexport:
   #+INCLUDE: "../../common/modules/principles-compact.org::#principled" :only-contents t :minlevel 3
** The largest software archive, a shared infrastructure
#+INCLUDE: "../../common/modules/principles-compact.org::#sharedinfra" :only-contents t :minlevel 3
** A peek under the hood: a universal archive
#+INCLUDE: "../../common/modules/under-the-hood-pictures.org::#automation" :only-contents t :minlevel 3
** Referencing all source code artifacts with SWHIDs
#+INCLUDE: "../../common/modules/swh-ardc.org::#swh-r-common" :only-contents t :minlevel 3
** A quick tour                                                   :noexport:
   #+INCLUDE: "../../common/modules/swh-ardc.org::#demoswhhal" :only-contents t :minlevel 3
** A revolutionary infrastructure for industry                    :noexport:
   #+INCLUDE: "../../common/modules/swh-as-infrastructure.org::#industry" :only-contents t :minlevel 3
** A revolutionary infrastructure for research and innovation     :noexport:
   #+INCLUDE: "../../common/modules/swh-as-infrastructure.org::#science" :only-contents t :minlevel 3
** The Software Heritage archive as an open dataset
1. All the file contents (the leaves of the graph ~1.5 PiB uncompressed)
2. Regular dumps of the graph (with all metadata, in ORC file format)
   #+BEGIN_EXPORT latex
   \begin{thebibliography}{Foo Bar, 1969}
   \scriptsize
   \bibitem{Pietri2019} Antoine Pietri, Diomidis Spinellis, Stefano Zacchiroli\newblock
   The Software Heritage Graph Dataset: Public software development under one roof\newblock
   MSR 2019: 16th Intl. Conf. on Mining Software Repositories. IEEE
   \end{thebibliography}
   #+END_EXPORT
***                                                              :B_column:
:PROPERTIES:
:BEAMER_env: column
:BEAMER_COL: .4
:END:
*Self-hosted* (10-20 TiB)
[[https://docs.softwareheritage.org/devel/swh-dataset/graph/dataset.html][docs.softwareheritage.org/devel/swh-dataset/graph/dataset.html]]
***                                                              :B_column:
:PROPERTIES:
:BEAMER_env: column
:BEAMER_COL: .4
:END:
#+ATTR_LATEX: :width .95\linewidth
file:aws-swh-registry.png
*Hosted on public clouds*
\footnotesize
[[https://registry.opendata.aws/software-heritage/][registry.opendata.aws/software-heritage]]

** Selected research works using Software Heritage
  #+BEGIN_EXPORT latex
  \begin{thebibliography}{Foo Bar, 1969}
  \scriptsize

  \bibitem{Barahona2023} Jesús M. González-Barahona, Sergio Raúl Montes León, Gregorio Robles, Stefano Zacchiroli
  \newblock The Software Heritage license dataset (2022 edition)
  \newblock Empir. Softw. Eng. 28(6): 147 (2023)

  \bibitem{Lefeuvre2023} Romain Lefeuvre, Jessie Galasso, Benoît Combemale, Houari A. Sahraoui, Stefano Zacchiroli:
  \newblock Fingerprinting and Building Large Reproducible Datasets
  \newblock ACM-REP 2023: 27-36

  \bibitem{Rossi2022} Davide Rossi, Stefano Zacchiroli
  \newblock Worldwide Gender Differences in Public Code Contributions [...]
  \newblock ICSE SEIS 2022: The 44th International Conference on Software Engineering

  %\bibitem{Serafini2022} Daniele Serafini, Stefano Zacchiroli
  %\newblock Efficient Prior Publication Identification for Open Source Code
  %\newblock OSS+OpenSym 2022: 18th International Conference on Open Source Systems

  %\bibitem{Allancon2021} Thibault Allançon, Antoine Pietri, Stefano Zacchiroli
  %\newblock The Software Heritage Filesystem (SwhFS): Integrating Source Code Archival with Development
  %\newblock ICSE 2021: The 43rd International Conference on Software Engineering

  \bibitem{Pietri2020a} Antoine Pietri, Guillaume Rousseau, Stefano Zacchiroli
  \newblock Forking Without Clicking: on How to Identify Software Repository Forks
  \newblock MSR 2020: 17th Intl. Conf. on Mining Software Repositories. IEEE

  \bibitem{Boldi2020} Paolo Boldi, Antoine Pietri, Sebastiano Vigna, Stefano Zacchiroli
  \newblock Ultra-Large-Scale Repository Analysis via Graph Compression
  \newblock SANER 2020, 27th Intl. Conf. on Software Analysis, Evolution and Reengineering. IEEE

  \bibitem{Rousseau2020} Roberto Di Cosmo, Guillaume Rousseau, Stefano Zacchiroli
  \newblock Software Provenance Tracking at the Scale of Public Source Code
  \newblock Empirical Software Engineering 25(4): 2930-2959 (2020)

  \end{thebibliography}
  #+END_EXPORT

** Industry use cases (selection)
*** Open Source complete and corresponding source code distribution \hfill (Intel)
    Software Heritage members can:
    - *archive* source code in Software Heritage, *distribute* only the *SWHID*
#+BEAMER: \pause
*** Traceability and integrity \hfill     (OIN for the /Linux System Definition/)
    Software Heritage members can:
    - *archive* source code in Software Heritage
    - *track* it and verify its *integrity* using its *SWHID*
#+BEAMER: \pause
*** And much more!
    - cybersecurity: just launched SWHSec project [[https://swhsec.github.io][swhsec.github.io]]
    - AI: providing high-quality data for ethical code LLMs
    - an open (source & data) source code scanner for open compliance
    # - supply chain management, long term archive \hfill /add your use case here/
#+INCLUDE: "../../common/modules/swh-scanner.org::#swh-scanner-one-slide" :minlevel 2
* Conclusion
** Reproducible Builds $\leftrightarrow$ Software Heritage
*** 
    - Software Heritage provides key ingredients for R-B pipelines: on-demand
      archival (e.g., of VCS commits referenced by build recipes) + long-term
      availability
    - We have implemented this by integrating the GNU Guix package manager with
      Software Heritage

***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    #+BEAMER: \begin{center}\hfill\includegraphics[height=0.4\textheight]{swh-guix-1}\hfill\includegraphics[height=0.4\textheight]{swh-guix-2}\hfill~\end{center}
    # - \url{https://www.softwareheritage.org/2019/04/18/software-heritage-and-gnu-guix-join-forces-to-enable-long-term-reproducibility/}
    # - \url{https://guix.gnu.org/blog/2019/connecting-reproducible-deployment-to-a-long-term-source-code-archive/}
    #+BEGIN_EXPORT latex
    \begin{thebibliography}{Foo Bar, 1969}
    \scriptsize
    \bibitem{Rousseau2020} Ludovic Courtès, Timothy Sample, Simon Tournier, Stefano Zacchiroli
    \newblock Source Code Archiving to the Rescue of Reproducible Deployment.
    \newblock ACM REP 2024 (to appear)
    \end{thebibliography}
    #+END_EXPORT
    
** Learn more
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .42
    :END:
#+BEAMER: \centering
#+ATTR_LATEX: :width .9\linewidth
[[file:r-b-logo.png]]
[[https://reproducible-builds.org/][reproducible-builds.org]]
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
#+BEAMER: \centering
#+ATTR_LATEX: :width .9\linewidth
[[file:SWH-logo+motto.png]]
[[https://www.softwareheritage.org/][softwareheritage.org]]

*** 
  #+BEGIN_EXPORT latex
  \begin{thebibliography}{Foo Bar, 1969}
  \footnotesize

  \bibitem{Ladis2022} Piergiorgio Ladisa, Henrik Plate, Matias Martinez, Olivier Barais
  \newblock SoK: Taxonomy of Attacks on Open-Source Software Supply Chains
  \newblock IEEE S\&P 2023

  \bibitem{Lamb2022} Chris Lamb, Stefano Zacchiroli
  \newblock Reproducible Builds: Increasing the Integrity of Software Supply Chains
  \newblock IEEE Softw. 39(2): 62-70 (2022)

  \bibitem{DiCosmo2017} Roberto Di Cosmo, Stefano Zacchiroli
  \newblock Software Heritage: Why and How to Preserve Software Source Code
  \newblock iPRES 2017: Intl. Conf. on Digital Preservation

  \end{thebibliography}
  #+END_EXPORT
* Appendix                                                       :B_appendix:
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
** 
   \vfill
   \centerline{\Huge Appendix}
   \vfill
* Reproducible Builds
#+INCLUDE: "../../common/modules/reproducible-builds.org::#r-b-challenges" :minlevel 2
* Software Heritage
** An international, non profit initiative\hfill built for the long term
#+INCLUDE: "../../common/modules/support+sponsors.org::#support+sponsors-pause" :only-contents t :minlevel 3
* Software Heritage --- Datasets
** A peek at the dataset
*** Accessing graph leaves (a.k.a. contents)   
   #+BEGIN_SRC bash
   $ aws s3 ls --no-sign-request s3://softwareheritage/
                          PRE content/
                          PRE graph/
   #+END_SRC
   #+BEAMER: \pause
   File contents can be accessed using their SHA1 checksum
   #+LATEX: {\scriptsize
   #+BEGIN_SRC bash
      $ aws s3 cp --no-sign-request \
	s3://softwareheritage/content/8624bcdae55baeef00cd11d5dfcfa60f68710a02 .
   #+END_SRC
   #+LATEX: }
   Notice that file contents are compressed:
   #+LATEX: {\scriptsize
   #+BEGIN_SRC bash
	 $ zcat 8624bcdae55baeef00cd11d5dfcfa60f68710a02 | head
			   GNU GENERAL PUBLIC LICENSE
			      Version 3, 29 June 2007

	 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
	 Everyone is permitted to copy and distribute verbatim copies
	 of this license document, but changing it is not allowed.
   #+END_SRC
   #+LATEX: }
** A peek at the dataset, cont'd                                   :noexport:
*** Annual dumps of (inner nodes of) the full graph
   #+LATEX: {\tiny
   #+BEGIN_SRC bash
   $ aws s3 ls --no-sign-request s3://softwareheritage/graph/
   #+END_SRC
   #+LATEX: \mbox{}\\[-4em]}
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .4
     :END:
   #+LATEX: {\tiny
   #+BEGIN_SRC bash
                           PRE 2018-09-25/
                           PRE 2019-01-28-popular-3k-python/
                           PRE 2019-01-28-popular-4k/
                           PRE 2020-05-20/
                           PRE 2020-12-15/
   #+END_SRC
   #+LATEX: }
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .4
     :END:
   #+LATEX: {\tiny
   #+BEGIN_SRC bash
                           PRE 2021-03-23-cpython-3-5/
                           PRE 2021-03-23-popular-3k-python/
                           PRE 2021-03-23/
                           PRE 2022-04-25/
   #+END_SRC
   #+LATEX: }
*** 
     :PROPERTIES:
     :BEAMER_env: ignoreheading
     :END:
*** How to use and cite
    - [[https://docs.softwareheritage.org/devel/swh-dataset/graph/index.html][online full documentation]], and read [[https://tel.archives-ouvertes.fr/tel-03515795v1][Antoine Pietri's PhD Thesis]]
    - Antoine Pietri, Diomidis Spinellis, Stefano Zacchiroli.
      /The Software Heritage Graph Dataset: Public software development under one roof./
      MSR 2019. ([[https://dblp.org/rec/conf/msr/PietriSZ20.html?view=bibtex][bibtex]])
    #+BEAMER: \pause
** A peek at the dataset, cont'd                                   :noexport:
*** Annual dumps of (inner nodes of) the full graph
   #+LATEX: {\small
   #+BEGIN_SRC bash
   $ aws s3 ls --no-sign-request s3://softwareheritage/graph/
   #+END_SRC
   #+BEGIN_SRC bash
                           PRE 2018-09-25/
                           PRE 2019-01-28-popular-3k-python/
                           PRE 2019-01-28-popular-4k/
                           PRE 2020-05-20/
                           PRE 2020-12-15/
                           PRE 2021-03-23-cpython-3-5/
                           PRE 2021-03-23-popular-3k-python/
                           PRE 2021-03-23/
                           PRE 2022-04-25/
   #+END_SRC
   #+LATEX: }
*** How to use
    - [[https://docs.softwareheritage.org/devel/swh-dataset/graph/index.html][online full documentation]]
    - [[https://tel.archives-ouvertes.fr/tel-03515795v1][Antoine Pietri's PhD Thesis]]
*** How to cite      
    Antoine Pietri, Diomidis Spinellis, Stefano Zacchiroli.
    /The Software Heritage Graph Dataset: Public software development under one roof./
    MSR 2019. ([[https://dblp.org/rec/conf/msr/PietriSZ20.html?view=bibtex][bibtex]])
** A peek at the dataset, cont'd
*** Annual dumps of (inner nodes of) the full graph
   #+LATEX: \small
   #+BEGIN_SRC bash
   $ aws s3 ls --no-sign-request s3://softwareheritage/graph/
   #+END_SRC
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .4
     :END:
   #+LATEX: {\small
   #+BEGIN_SRC bash
                           2018-09-25/
                           2019-01-28-popular-3k-python/
                           2019-01-28-popular-4k/
                           2020-05-20/
                           2020-12-15/
   #+END_SRC
   #+LATEX: }
****                                                               :B_column:
     :PROPERTIES:
     :BEAMER_env: column
     :BEAMER_COL: .4
     :END:
   #+LATEX: {\small
   #+BEGIN_SRC bash
                           2021-03-23-cpython-3-5/
                           2021-03-23-popular-3k-python/
                           2021-03-23/
                           2022-04-25/
   #+END_SRC
   #+LATEX: }
*** 
     :PROPERTIES:
     :BEAMER_env: ignoreheading
     :END:
*** How to use
    - [[https://docs.softwareheritage.org/devel/swh-dataset/graph/index.html][online full documentation]]
    - [[https://tel.archives-ouvertes.fr/tel-03515795v1][Antoine Pietri's PhD Thesis]]
*** How to cite      
    Antoine Pietri, Diomidis Spinellis, Stefano Zacchiroli.
    /The Software Heritage Graph Dataset: Public software development under one roof./
    MSR 2019. ([[https://dblp.org/rec/conf/msr/PietriSZ20.html?view=bibtex][bibtex]])
** Example: most popular commit verbs (stemmed)
*** Query using Amazon Athena                                                             :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .4
    :END:
   \tiny
   #+begin_src sql
     SELECT COUNT(*) AS C, word FROM (
	SELECT word_stem(lower(split_part(
 	 trim(from_utf8(message)),' ', 1)))
	AS word FROM revision
        WHERE length(message) < 1000000)
     WHERE word != ''
     GROUP BY word
     ORDER BY C
     DESC LIMIT 20;
   #+end_src
   \hfill /Total cost: approximately .5 euros/
    #+BEAMER: \pause
*** Results                                                         :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .6
    :END:
    #+ATTR_LATEX: :width \linewidth
    file:stemverbs.png
* Efficient traversal of the full graph
** Going beyond SQL
*** State-of-the-art graph compression from social networks   
   #+BEAMER: \vspace{-2mm}
    #+BEGIN_EXPORT latex
    \footnotesize
    \begin{thebibliography}{Foo Bar, 1969}
    \bibitem{Boldi2020} Paolo Boldi, Antoine Pietri, Sebastiano Vigna, Stefano Zacchiroli
    \newblock Ultra-Large-Scale Repository Analysis via Graph Compression
    \newblock SANER 2020, 27th Intl. Conf. on Software Analysis, Evolution and Reengineering. IEEE
    \end{thebibliography}
    #+END_EXPORT
*** Results    
   Full graph structure (25 B nodes, 350 B edges) in 200 GiB RAM
    - traversal time is tens of ns per edge
    - bidirectional traversals implemented
    - *beware:* metadata access is still /off RAM/
*** Java and gRPC APIs available
    \hfill\small [[https://docs.softwareheritage.org/devel/swh-graph/grpc-api.html][docs.softwareheritage.org/devel/swh-graph/grpc-api.html]]
** Examples \hfill /assume graph service on localhost:50091/
*** Find all origins containing a given content
     \scriptsize
    #+begin_src bash
    grpc_cli call localhost:50091 swh.graph.TraversalService.Traverse "\
    src: 'swh:1:cnt:8722d84d658e5e11519b807abb5c05bfbfc531f0', direction: BACKWARD, \
    mask: {paths: ['swhid','ori.url']}, return_nodes: {types: 'ori'}" 
    #+end_src
    Gives a list of origins including "https://github.com/rdicosmo/parmap", encoded as
    "swh:1:ori:8903a90cff8f07159be7aed69f19d66d33db3f86" (*beware*: this is *not* a SWHID!)
#+BEAMER: \pause    
*** Shortest provenance path of a content in a given origin
     \scriptsize
    #+begin_src bash
      grpc_cli call localhost:50091 swh.graph.TraversalService.FindPathBetween "\
      src: 'swh:1:ori:8903a90cff8f07159be7aed69f19d66d33db3f86', \
      dst: 'swh:1:cnt:8722d84d658e5e11519b807abb5c05bfbfc531f0', \
      mask: {paths: ['swhid']}" | egrep 'swhid'
      connecting to localhost:50091
	swhid: "swh:1:ori:8903a90cff8f07159be7aed69f19d66d33db3f86"
	swhid: "swh:1:snp:1527a93b039d70f6a781b05d76b77c6209912887"
	swhid: "swh:1:rev:82df563aecf86b9164eee7d10d40f2d8cbd1c78d"
	swhid: "swh:1:dir:484db39bb2825886191837bb0960b7450f9099bb"
	swhid: "swh:1:dir:4d15e44b378fe39dd23817abee756cd47ad14575"
	swhid: "swh:1:cnt:8722d84d658e5e11519b807abb5c05bfbfc531f0"
      Rpc succeeded with OK status
    #+end_src
