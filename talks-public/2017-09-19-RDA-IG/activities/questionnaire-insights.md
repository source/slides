Participants are interested in this subject for various reasons:
- cite software
- recover software
- data needs software (without software, data is basically useless)
- discover software
- provide framework for better software discovery and research   
- reuse software (with environment)
- manage software
- preserve software
- software as a first class research product
- describe properly software
- PID for software
- identify and incorporate better practices for software
- software provenance

Use cases:
- discover software by searching for specific interest
    - domain/area
    - algorithm/ functionality
    - data provided (software producing a certain result
        with a particular data set)
    - environment (software used in particular environment-distro,
        compiler, etc)
    - conditions for use, reuse and modification
- publish/ deposit/ archive software with associated metadata
- link software artifact to its context:
    - data
    - people/ authorship
    - funding
    - dependencies
    - built form
- cite software and give due credit
- integrate software to other workflow
    - reproduce software
        - discover dependencies and environment needed


Ontologies used:
- most don't or use data ontologies
- Datacite
- CodeMeta
- DublinCore
- package management (NPM, gemspec, PYPI)


Properties needed:
- PID
- maintainer email
- link to compiled version
- repository retrieval link
- data input/output expected
- authorship & affiliation
- version
- description
- references
- origin  source (for provenance)
- type
- description / algorithms / problem solved
- language
- revisions+ dates
- funders
- is documented & documentation link
- terms of use / license
- dependencies
- compiler
- environment (compiles / run on)
- status
- examples
- related_to (relations to other software)
- publisher
- tests link & test data

Advantages for structured and linked data:
- help scientists discover software - better research
- better connection with data
- better credit (by linking to authors)
- backward and forward linking- better ecosystem
