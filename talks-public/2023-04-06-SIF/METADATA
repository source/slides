Title: Open Source Challenges and Opportunities: From Open Science to the Software Supply Chain

Abstract:

Open Science is a powerful force that has a profound and lasting impact on how we conduct, share, and evaluate research. Open Source software plays a critical role in this context, just like open access to research articles and data, and it presents specific challenges. National and international efforts are underway to preserve, reference, share, trace, and cite all publicly accessible source codes.

Interestingly, some of these needs are common with the software industry. Open Source has introduced a new dimension to software security by providing access to large amounts of collaboratively developed and distributed software components from around the world. This has significantly accelerated innovation but also posed significant challenges regarding the quality, evolution, and security of modern software systems' "software supply chain," where various components from different sources are combined.

How do we search for vulnerabilities among millions of software projects? How can we track their propagation? How can we ensure that the source code of a critical module used today will still be available when needed in the future? Do we truly know what source code we are using and where it comes from (the "Know Your Software" principle)? How can we address cybersecurity if we do not know? How do we share this information across the software supply chain?

Addressing these questions on a large scale is quite a challenge. In this presentation, you will learn about Software Heritage, an open non-profit initiative in partnership with UNESCO and supported by major IT players. You will also discover how the revolutionary infrastructure it is building offers significant opportunities to change how we approach these issues.

With over 14 billion unique source files from more than 210 million repositories, Software Heritage is the most extensive archive of source code ever constructed. You can already access and utilize it for a variety of purposes.
