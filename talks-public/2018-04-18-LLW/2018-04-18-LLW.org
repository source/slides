#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Outsourcing Source Code Distribution Requirements
#+AUTHOR: Alexios Zavras, Roberto Di Cosmo, Stefano Zacchiroli
#+BEAMER_HEADER: \date[LLW 2018]{18 April 2018\\LLW\\Barcelona}
#+DATE: 18 April 2018

#+INCLUDE: "this/prelude-swh+intel.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+BEAMER_HEADER: \institute[Intel \& Software Heritage]{Intel, alexios.zavras@intel.com\\Software Heritage, roberto@dicosmo.org, zack@upsilon.cc}
#+BEAMER_HEADER: \setbeamertemplate{background}{\includegraphics[width=\paperwidth,height=\paperheight]{world-169}}

#+LATEX_HEADER_EXTRA: \usepackage{tikz}
#+LATEX_HEADER_EXTRA: \usetikzlibrary{arrows,shapes}
#+LATEX_HEADER_EXTRA: \definecolor{swh-orange}{RGB}{254,205,27}
#+LATEX_HEADER_EXTRA: \definecolor{swh-red}{RGB}{226,0,38}
#+LATEX_HEADER_EXTRA: \definecolor{swh-green}{RGB}{77,181,174}
#
# to have a toc for each section
#
# Use suggestions from http://web.stanford.edu/~dgleich/notebook/2009/05/appendix_slides_in_beamer_cont_1.html to avoid counting tocs in page number
#
#+latex_header: \AtBeginSection[] {\begin{frame}<*> \frametitle{Outline} \tableofcontents[currentsection]\end{frame} \addtocounter{framenumber}{-1}}

* Real-world compliance
  #+INCLUDE: "this/intel-use-case.org::#main" :minlevel 2 :only-contents t
* Software Heritage
** The Software Heritage Project \hfill www.softwareheritage.org
  :PROPERTIES:
  :CUSTOM_ID: mission
  :END:
#+latex: \begin{center}
#+ATTR_LATEX: :width .8\linewidth
# file:SWH-logo+motto.pdf
file:SWH-logo.pdf
#+latex: \end{center}
*** Our mission
      *Collect*, *preserve* and *share* the /source code/ of /all the software/\\
\mbox{}\\
     \hfill /Preserving/ the past, /enhancing/ the present, /preparing/ the future
*** Going global						   :noexport:
    \hfill building an /open, multistakeholder, nonprofit/ organisation

** Archive and observatory, serving the needs of society as a whole
#+latex: \begin{center}
#+ATTR_LATEX: :width .6\linewidth
file:SWH-as-foundation-slim.png
#+latex: \end{center}
#+BEGIN_EXPORT latex
\note{On top of Software Heritage one can imagine a myriad applications, for education,
research, industry, cultural heritage, and society as a whole.\\[1em]
But building the universal archive of source code geared towards the long term is a grand challenge 
on its own: so we follow the Unix phylosophy, and focus on doing one thing, and doing it well,
building this essential infrastructure for software.\\[1em]
Our principles are simple: all our code is and will be open source, our organisation is transparent;\\[1em]
and we focus on the long term: we will grow an international network of mirrors and partners
and create a non profit foundation to coordinate it for the benefit of society as a whole.}
#+END_EXPORT

\begin{center}
 \includegraphics[width=.7\linewidth]{growth.png}
\end{center}
*** 
    \hfill largest collection of software source code in the world
** Architecture (simplified)
    #+BEAMER: \begin{center}
    #+BEAMER:   \mode<beamer>{\only<1>{\includegraphics[width=\extblockscale{.9\textwidth}]{swh-dataflow-merkle-listers.pdf}}}
    #+BEAMER:   \only<2-3>{\includegraphics[width=\extblockscale{.9\textwidth}]{swh-dataflow-merkle.pdf}}
    #+BEAMER: \end{center}
#+BEAMER: \pause
#+BEAMER: \pause
   - crawling + normalizing
   - full development history permanently archived
   - origins: GitHub (automated), Debian (automated), Gitorious, Google Code, GNU, ...
   - ~150Tb raw contents, ~10Tb graph (7+Bn nodes, 60+Bn edges)
** Growing Support
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Landmark Inria Unesco agreement, April 3rd, 2017
 #+BEGIN_EXPORT latex
    \includegraphics[width=\extblockscale{.25\linewidth}]{inria-logo-new} \hfill
    \includegraphics[width=\extblockscale{.35\linewidth}]{unesco-accord} \hfill
    \includegraphics[width=\extblockscale{.2\linewidth}]{unesco}\\[1em]
 #+END_EXPORT
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .5
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
*** See more                                                       :noexport:
    \hfill\tiny\url{http:://www.softwareheritage.org/support/testimonials}
*** Contributing to the mission                                     :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
    #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.4\linewidth}]{inria-logo-new}\end{center}
   #+LATEX: \begin{center}
   #+LATEX: \includegraphics[width=\extblockscale{.2\linewidth}]{sponsors-levels.pdf}
   #+LATEX: \includegraphics[width=\extblockscale{1.1\linewidth}]{sponsors.pdf}
   #+LATEX: \end{center}
#   - sponsoring / partnership :: \hfill \url{sponsorship.softwareheritage.org}
** Relevant features for source code distribution
*** Intrinsich identifiers
    - contents, directories, releases, etc. all get a hash (SHA1)
#+BEAMER: \pause
*** Download from the Software Heritage Archive
    - given a hash, extract tarball/commit 
#+BEAMER: \pause
*** Deposit into the Software Heritage Archive
    open to /selected/ partners:
    - /open access archives/
    - /industry sponsors/
#+BEAMER: \pause
*** Automation
    - all of the above can be /scripted/ \hfill ...details on demand (see Zack!)
#+BEAMER: \pause
*** 
    \hfill Questions?
* A worked example
  #+INCLUDE: "../../common/modules/deposit.org::#overview" :minlevel 2
  #+INCLUDE: "../../common/modules/deposit.org::#prepare" :minlevel 2
  #+INCLUDE: "../../common/modules/deposit.org::#send" :minlevel 2
  #+INCLUDE: "../../common/modules/deposit.org::#status" :minlevel 2
  #+INCLUDE: "../../common/modules/deposit.org::#access" :minlevel 2
  #+INCLUDE: "../../common/modules/vault.org::#vault-short" :minlevel 2 :only-contents t
** Wrapping up
*** 
    - long-term _hosting of CCS_ archives can be onerous in the real-world
    - it is A-OK to _outsource_ that responsibility to third parties
    - Software Heritage crawls (pull) _all FOSS_ and can now accept push deposits
    - Intel and Software Heritage are working together on _practical FOSS
      tooling_ to outsource CCS hosting to the Software Heritage archive
*** Come and join us!
    - =alexios.zavras@intel.com= , =roberto@dicosmo.org=, =zack@upsilon.cc=
    - https://www.softwareheritage.org
    - https://deposit.softwareheritage.org
    - https://archive.softwareheritage.org (FOSDEM 2018 preview!)
