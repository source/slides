** A (brief) research roadmap --- 1
*** Graph compression
    - incremental, amortized compression → ongoing UniMi collaboration
    - graph query languages on top of the compressed representation → LIRIS
      collaboration (early stages)
  #+BEAMER: \vfill \pause
*** Complex networks
    - local topology of the global VCS graph
    - emergent properties (the "classics": scale-free, small world, etc.)
    - dynamic modeling of graph evolution over time → collab. with physics @
      UParis
*** 
    #+BEGIN_EXPORT latex
    \vspace{-2mm}
    \begin{thebibliography}{}
    \small
    \bibitem{Pietri2019}  Antoine Pietri, Guillaume Rousseau, Stefano Zacchiroli\newblock
    Determining the Intrinsic Structure of Public Software Development History\newblock
    MSR 2020: 17th Intl. Conf. on Mining Software Repositories. IEEE\newblock
    registered study protocol
    \end{thebibliography}
    #+END_EXPORT
** A (brief) research roadmap --- 2
  #+BEAMER: \vspace{-2mm}
*** Very-large-scale "big code"
    - /big code/ = apply ML/DL to source code and other development byproducts
    - current results are language-specific and limited in scale; even the
      simplest problems become challenging at this scale and heterogeneity
      - lead: scalable language detection → collaboration with UniBo
      - lead: project classification → collaboration with CELI
    - the VCS graph remains largely unexplored in big code
      - lead: use GNN for VCS node classification → ANR COREOGRAPHIE
    #+BEAMER: \vspace{-1mm}
***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    #+BEAMER: \vspace{-1mm} \pause
*** Very-large-scale source code indexing
    - common AST-based approaches for code indexing are not viable here due do
      maximum heterogeneity
    - alternative: treat code as text and full-text index it
      - previous exp.: 3-gram based indexing in Debsources, supporting regexp
        matching
    - goal: find a sweet spot between the two
    #+BEAMER: \vspace{-1mm}
** Recent results on (visual) programming language identification  :noexport:
   #+BEGIN_EXPORT latex
   \begin{thebibliography}{}
   \small

   \bibitem{DelBonifro2021a} Francesca Del Bonifro, Maurizio Gabbrielli, Stefano Zacchiroli
   \newblock Content-Based Textual File Type Detection at Scale
   \newblock ICMLC 2021: The 13th International Conference on Machine Learning and Computing. ACM, 2021 

   \bibitem{DelBonifro2021b} Francesca Del Bonifro, Maurizio Gabbrielli, Antonio Lategano, Stefano Zacchiroli
   \newblock Image-based many-language programming language identification
   \newblock (under review)

   \end{thebibliography}
   #+END_EXPORT
** A (brief) research roadmap --- 3
*** Very-large-scale reproducibility in software engineering
    - most results in empirical software engineering are determined on corpuses
      significantly smaller than Software Heritage\\
      → external validity threat; do results generalize to the full body of
      public code?
    - 2-year research plan
      1) identify impactful sw. eng. studies that can be reproduced using
         Software Heritage
	 - selected topics (tentative): code reuse, code quality, project
           classification, technical debt, developer productivity
      2) reproduce selected studies one-by-one, at Software Heritage scale
      3) document findings, e.g., via RENE (REproducibility Studies and
         NEgative Results) scientific initiatives
    - collaboration with Microsoft Research (just started)
