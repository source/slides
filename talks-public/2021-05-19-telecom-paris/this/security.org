** Securing the open source supply chain

   *Software supply chain attacks* are becoming more and more popular and
   raising in profile. → Cf. /SolarWindws attacks/ (2021), breaching several US
   govt. branches

*** Definition --- Reproducible Builds (R-B)
    The build process of a software product is *reproducible* if, after
    designating a specific version of its source code and all of its build
    dependencies, every build produces *bit-for-bit identical artifacts*, no
    matter the environment in which the build is performed.

*** 
    - R-B allows to *increase trust in binary executables* built from trusted
      (open source) code by untrusted 3rd-party software vendors (e.g., app
      stores, distros)

    - The *[[https://reproducible-builds.org/][reproducible-builds.org project]]* has popularized the notion, is
      backed by major open source industry players, and has made large open
      source software collections reproducible (e.g., 95% of Debian packages)

*** References                                              :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    #+BEGIN_EXPORT latex
    \begin{thebibliography}{}
    \footnotesize
    \bibitem{Lamb2021RB} Chris Lamb, Stefano Zacchiroli 
    \newblock Reproducible Builds: Increasing the Integrity of Software Supply 
    \newblock IEEE Software 2021 (to appear, DOI 10.1109/MS.2021.3073045)
    \end{thebibliography}
    #+END_EXPORT

** Securing the open source supply chain (cont.)
   #+BEAMER: \begin{center}\includegraphics[width=\textwidth]{this/r-b-approach}\end{center}

** Securing the open source supply chain (cont.)
*** 
    - Software Heritage provides key ingredients for R-B pipelines: on-demand
      archival (e.g., of VCS commits referenced by build recipes) + long-term
      availability
    - We have implemented this by integrating the GNU Guix package manager with
      Software Heritage

***                                                         :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
    #+BEAMER: \begin{center}\hfill\includegraphics[height=0.4\textheight]{swh-guix-1}\hfill\includegraphics[height=0.4\textheight]{swh-guix-2}\hfill~\end{center}
    #+BEAMER: \scriptsize
    - \url{https://www.softwareheritage.org/2019/04/18/software-heritage-and-gnu-guix-join-forces-to-enable-long-term-reproducibility/}
    - \url{https://guix.gnu.org/blog/2019/connecting-reproducible-deployment-to-a-long-term-source-code-archive/}

** Tracking of vulnerable source code artifacts

*** 
    Software Heritage provides a unique observatory on the (best approximation
    of) the entire /Software Commons/, i.e., all software published in source
    code form

*** Software provenance tracking at the scale of the world
    - by following the /transposed/ Software Heritage graph we can locate *all
      known public occurrences* of source code artifacts (individual source
      files, entier source tree, commits) in other commits or repositories

    - we have developed two approaches to do that:

      1. database-based (Rousseau et al. EMSE 2020): incremental, answers a
         fixed set of queries, requires significant disk space

      2. compressed-graph-based (Boldi et al. SANER 2020): non-incremental,
         flexible graph-base querying, fits in RAM

    - current applications: "intellectual property"/prior art, open source
      license compliance, software composition analysis (SCA) → collab. with
      CAST

** Tracking of vulnerable source code artifacts (cont.)

*** Adding in-memory commit timestamps (experimental)
    Idea: in-memory timestamp array (us precision, 8 bytes each), indexed by
    revision node id. This enables to efficiently exploit timestamp information
    during graph visits.

*** Finding the /earliest/ commit referencing a source file/dir
    Early experiment: finding the earliest revision containing a given file
    using in-memory commit timestamps, on 10 M randomly selected blobs.

    Mean lookup time: 4.1 ms (avg on 95% percentile: 2.2 ms)

*** Tracking vulnerable source code files/trees
    Given a source file/tree affected by a known vulnerability (e.g.,
    identified by a CVE) we can efficiently identify /all/ commits (and
    repositories, extending the traversals) that reference it, triggering
    further inspection. Furthermore, we can efficiently select which commits to
    filter out during visits (e.g., "recent" ones, only in selected repos,
    etc.), based on timestamps of other attributes (that fit in memory or are
    mmap()-ed to disk).

** Tracking of vulnerable source code artifacts (cont.)

*** v. State-of-the-art industry offerings
    Similar to what GitHub/GitLab offer as a service, but:

    - without having to rely on repository scanning, because the "big picture"
      is already present in the Software Heritage archive by design

    - independent from the development platform vendor (e.g., a "vulnerable
      file" primarily hosted on GitHub can be spotted in GitLab repositories
      and vice-versa)

    - complementary and synergistic with analyses of vulnerable dependency
      information (which are also available in Software Heritage via metadata
      mining)

*** Caveats

    - current granularity stops at the file level and traceability breaks with
      even just whitespace changes. Increasing tracking granularity to the
      snippet/line of code level is possible, but untested at this scale yet
      (cf. research roadmap)
