
** Short Bio: Stefano Zacchiroli
  :PROPERTIES:
  :CUSTOM_ID: bio
  :END:
*** 
    - Professor of Computer Science, Télécom Paris, Institut Polytechnique de
      Paris
    - Free/Open Source Software activist (20+ years)
    - Debian Developer & Former 3x Debian Project Leader
    - Former Open Source Initiative (OSI) director
    - Software Heritage co-founder & CTO
