Title: Securing the (Open Source) Software Supply chain: challenges and opportunities

Abstract:

Software security is a broad and well established domain, with many research
projects and industry products focusing on the traditional goal of detecting,
and possibly fixing, vulnerabilities present in a specific software code base.
Open Source has brought a new dimension into play: by making available large
amounts of software components, developed and distributed collaboratively across
the world, it has immensely sped up innovation, but also raised key challenges
about the quality, evolution and security of what is today called the "software
supply chain" of modern software systems, where many components from disparate
origins are put together.

How can we search for vulnerabilities among millions of software projects?  How
can we track their propagation? How can one make sure that the source code of a
key module used today will be still there when it is needed in the future? Do we
really know what source code we are using, and where it comes from (the Know
Your Software principle)? how can we adress cybersecurity if we do not know? How
do we share this information across the software supply chain?

Answering these questions and answering them at scale is quite a challenge.

In this presentation, you will discover Software Heritage, an open non-profit
initiative, in partnership with Unesco and supported by major IT players, and
how the revolutionary infrastructure it is building offers great opportunities
to change the way we address these issues.

With more than 17 billions unique source files from more than 270 million
repositories, it is the largest archive of source code ever built, and
you can already access and use it for a variety of purposes.
