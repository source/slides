#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: EOSC-Pillart Use case 6.4
#+SUBTITLE: Software source code preservation, reference and access
#+DATE: 03/03/2020
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology
#+LATEX_HEADER: \usepackage{tcolorbox}
#+LATEX_HEADER: \definecolor{links}{HTML}{2A1B81}
#+LATEX_HEADER: \hypersetup{colorlinks,linkcolor=,urlcolor=links}
#
# prelude.org contains all the information needed to export the main beamer latex source
# use prelude-toc.org to get the table of contents
#

#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1


#+INCLUDE: "../../common/modules/169.org"

# +LaTeX_CLASS_OPTIONS: [aspectratio=169,handout,xcolor=table]
#+LATEX_HEADER: \usepackage{bbding}
#+LATEX_HEADER: \usepackage{tcolorbox}
#+LATEX_HEADER: \DeclareUnicodeCharacter{66D}{\FiveStar}


#
# If you want to change the title logo it's here
#
#+BEAMER_HEADER: \titlegraphic{\includegraphics[width=0.5\textwidth]{Inria-HAL-CCSD-SWH-logo-horizontal.png}}

# aspect ratio can be changed, but the slides need to be adapted
# - compute a "resizing factor" for the images (macro for picblocks?)
#
# set the background image
#
# https://pacoup.com/2011/06/12/list-of-true-169-resolutions/
#
#+BEAMER_HEADER: \pgfdeclareimage[height=90mm,width=160mm]{bgd}{swh-world-169.png}
#+BEAMER_HEADER: \setbeamertemplate{background}{\pgfuseimage{bgd}}
#+LATEX: \addtocounter{framenumber}{-1}
* Archiving and referencing /all/ the source code: Software Heritage
  #+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :minlevel 2
** Software Heritage: a revolutionary infrastructure \hfill bit.ly/swhpaper
  :PROPERTIES:
  :CUSTOM_ID: hilights
  :END:
*** /All/ the software source code
   #+latex: \centering
   #+latex: \mbox{}\hfill\includegraphics[width=\extblockscale{.35\linewidth}]{swh-dataflow-merkle.pdf}\hfill\pause
   #+latex: \includegraphics[width=\extblockscale{.75\linewidth}]{2019-09-archive-growth.png}\hfill\mbox{}\\
#+BEAMER: \pause
   The largest software source code archive /ever/ 
*** /Uniform and intrinsic/ identifiers for reproducibility         :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:
   Tracking over /20 billion software artifacts/, and counting... \hfill \url{bit.ly/swhpidpaper}
   #+BEAMER: \pause
* Use case description
** Use case 6.4 : description
*** Pilot based on Software Heritage 
    test the integration of /massive/ collections of /software source code/ into EOSC
*** Tasks
    - [X] develop a deposit API for research software archival,
    - [X] persistent identifier schema (PID) for referencing billions of archived software artifacts,
    - [ ] develop an access API for retrieval of archived software,
    - [ ] integrate the above APIs and Services with EOSC eTDR,
    - [ ] develop a pilot to fully host the software archive onto existing EOSC infrastructure.
* Status
** Deposit API for research software archival
*** the deposit workflow
    :PROPERTIES:
    :BEAMER_COL: .5
    :END:
    #+latex: \begin{center}
    #+ATTR_LATEX: :width \linewidth
    file:deposit-communication-with-PID.png
    #+latex: \end{center}
#+LATEX: \pause

*** Deposit software API v1.0                                    :B_picblock:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:    
    *\hspace{1em}Generic mechanism:*
    - SWORD based
    - versioning supported
#+BEAMER: \pause
    *\hspace{1em} Test phase on HAL:* \hfill  ([[http://bit.ly/swhdeposithalen][/guide/]])
    - review process
    - deposit .zip or .tar.gz file with metadata
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** Documentation
    - https://deposit.softwareheritage.org/
    - https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html
** Submit your source code  \hfill  ([[http://bit.ly/swhdeposithalen][/guide/]])                      :noexport:
#+latex: \begin{center}
#+ATTR_LATEX: :width \linewidth
file:HAL-form-IDCC.png
#+latex: \end{center}
** The deposit view
#+latex: \begin{center}
#+ATTR_LATEX: :width 0.7\linewidth
file:HAL_deposit.png
#+latex: \end{center}

