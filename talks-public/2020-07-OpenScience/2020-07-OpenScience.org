#+TITLE: Software Source Code in Research and Open Science
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+BEAMER_HEADER: \title[Archiving and Referencing source code~~~~~~~CC-BY 4.0]{Software Source Code in Research and Open Science}
#+AUTHOR: Roberto Di Cosmo
#+EMAIL: roberto@dicosmo.org
#+BEAMER_HEADER: \date[July 2020]{July 2020}
#+BEAMER_HEADER: \author[@rdicosmo~~~~~~Roberto Di Cosmo~~~~~~@swheritage]{Roberto Di Cosmo\\Director, Software Heritage}
#+KEYWORDS: software, heritage, legacy, preservation, knowledge, mankind, technology, prospective, software development
#
# prelude.org contains all the information needed to export the main beamer latex source
# use prelude-toc.org to get the table of contents
#
#+LATEX_HEADER: \usepackage{tcolorbox}
#+LATEX_HEADER: \definecolor{links}{HTML}{2A1B81}
#+LATEX_HEADER: \hypersetup{colorlinks,linkcolor=,urlcolor=links}
#+INCLUDE: "../../common/modules/prelude-toc.org" :minlevel 1

#+INCLUDE: "../../common/modules/169.org"
* Software Source Code is knowledge
** Software source code: /human readable/ and /executable knowledge/
  :PROPERTIES:
  :CUSTOM_ID: softwareisdifferent
  :END:
*** Harold Abelson, Structure and Interpretation of Computer Programs \hfill (1985)
    /“Programs must be written for people to read, and only incidentally for machines to execute.”/
#+BEAMER: \pause
*** Apollo 11 source code ([[https://archive.softwareheritage.org/swh:1:cnt:0c1741c1fb0150f111625d02277407f628c31bac;origin=https://github.com/virtualagc/virtualagc;visit=swh:1:snp:cdcd2bc43331a436e8c659ba93175ef7d7eb339b;anchor=swh:1:rev:4e5d304eb7cd5589b924ffb8b423b6f15511b35d;path=/Luminary116/THE_LUNAR_LANDING.agc;lines=244-260/][excerpt]])                     :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.48
    :BEAMER_env: block
    :END:
   #+LATEX: \includegraphics[width=\linewidth]{apollo-11-cranksilly.png}
   # excerpt of routine that asks astronaut to turn around the LEM
#+BEAMER: \pause
*** Quake III source code ([[https://archive.softwareheritage.org/swh:1:cnt:bb0faf6919fc60636b2696f32ec9b3c2adb247fe;origin=https://github.com/id-Software/Quake-III-Arena;visit=swh:1:snp:687ac8cdbfab3b78b7f301abee5f451127f135fc;anchor=swh:1:rev:dbe4ddb10315479fc00086f08e25d968b4b43c49;path=/;lines=549-572/][excerpt]])                           :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.45
    :BEAMER_env: block
    :END:
   #+LATEX: \includegraphics[width=\linewidth]{quake-carmack-sqrt-1.png}
   # smart efficient implementation of 1/sqrt(x) on a CPU without special support
#+BEAMER: \pause
*** 							    :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Len Shustek, Computer History Museum \hfill (2006)
    \hfill /“Source code provides a view into the mind of the designer.”/
   
*** Distinguishing features					   :noexport:
    - /executable/ and /human readable/ knowledge (an /all time new/)
      + even hardware is... software! (VHDL, FPGA, ...)
      + /text files are forever/
    - naturally /evolves/ over time
      + the /development history/ is key to its /understanding/
    - complex: large /web of dependencies/, millions of SLOCs
*** In a word							   :noexport:
    - software /is not just another/ sequence of bits
    - a software archive /is not just another/ digital archive
** Source code is /special/ (software is /not/ data)
*** Software /evolves/ over time
    - projects may last decades
    - the /development history/ is key to its /understanding/
#+BEAMER: \pause
*** Complexity :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=python3-matplotlib.pdf, width=.6\linewidth
    :END:
    - /millions/ of lines of code
    - large /web of dependencies/
      + easy to break, difficult to maintain
    - sophisticated /developer communities/
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** A vast part is not research software
    - industry and communities drive standards,
      build the necessary support layers
** Source code is /special/, cont'd
*** Versioning, granularity
   - Project :: “Inria created OCaml and Scikit-learn”\pause
   - Release :: “2D Voronoi Diagrams were introduced in CGAL 3.1.0”\pause
   - Precise state of a project :: “This result was produced using commit 0064fbd...”\pause
   - Code fragment :: “The core algorithm is in lines 101 to 143 of the file parmap.ml contained in the precise state of the project corresponding to commit 0064fbd....”
   #+BEAMER: \pause
*** Authors can have multiple roles:
   - Architecture, Management, Development, Documentation, Testing, ...
** Software Source code: pillar of Open Science
*** Three pillars of Open Science :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .4
    :END:
#+latex: \begin{center}
#+ATTR_LATEX: :width \extblockscale{1.2\linewidth}
file:PreservationTriangle.png
#+latex: \end{center}
#+BEAMER: \pause
*** A plurality of needs :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .6
    :END:
    - Researcher :: 
     - *archive* and *reference* software used in articles
     - *find* useful software
     - get *credit* for developed software
     - verify/reproduce/improve results
 #+BEAMER: \pause
    - Laboratory/team :: track software contributions
     - produce reports / web page
 #+BEAMER: \pause
    - Research Organization :: know its *software assets*
     - technology *transfer*
     - impact *metrics*
** What is at stake \hfill in increasing order of difficulty
*** Archival
    Research software artifacts must be properly *archived*\\
    \hfill make sure we can /retrieve/ them (/reproducibility/)
#+BEAMER: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-.5em}
*** Identification
    Research software artifacts must be properly *referenced*\\
    \hfill make sure we can /identify/ them (/reproducibility/)
#+BEAMER: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-.5em}
*** Metadata
    Research software artifacts must be properly *described*\\
    \hfill make it easy to /discover/ them (/visibility/)
#+BEAMER: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-.5em}
*** Citation
    Research software artifacts must be properly *cited* /(not the same as referenced!)/\\
    \hfill to give /credit/ to authors (/evaluation/!)
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
\vspace{-.5em}
#+BEAMER: \pause
**** 
    We need infrastructures /designed for/ software source code: \pause now we have one!
* Software Heritage
** Software Heritage, in a nutshell
#+BEAMER: \transdissolve
#+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :only-contents t :minlevel 3
** Addressing the four needs
*** Archive (8B+ files, 130M+ projects!)
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
#+ATTR_LATEX: :width .8\linewidth
   file:swh-dataflow-merkle.pdf
\vspace{-1em}
   - [[https://save.softwareheritage.org][save.softwareheritage.org]]
   - [[https://deposit.softwareheritage.org][deposit.softwareheritage.org]]
# (HAL, IPOL)
#+BEAMER: \pause
*** Reference (20 billion SWHIDs)                                   :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    [[https://www.softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers/][Intrinsic, decentralised, cryptographically strong identifiers, SWHIDs]]
\vspace{-1em}
#+ATTR_LATEX: :width 1.02\linewidth
    file:SWHID-v1.4_3.png
    Now supported [[https://www.softwareheritage.org/2020/05/13/swhid-adoption/][in SPDX 2.2, Wikidata]] etc.
#+BEAMER: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Describe                                                        :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - Intrinsic metadata from source code
    - Contributed the [[https://codemeta.github.io/codemeta-generator/][Codemeta generator]]
#+BEAMER: \pause
*** Cite                                                            :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - Contributed software citation style
      [[https://www.ctan.org/tex-archive/macros/latex/contrib/biblatex-contrib/biblatex-software][biblatex-software, v 1.2-2 now on CTAN]]
** Software Heritage for research                                  :noexport:
*** Archive
      - a /universal/ archive: collects /all/ software, not only academic software
      - /harvests/ source code worldwide /([[https://archive.softwareheritage.org][8B+ files from 130M+ projects in July 2020]])/
      - your software may be there already... if not, please [[https://save.softwareheritage.org][/save its code now/]]!
#+BEAMER: \pause
*** Reference
      - [[https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html][SWHID]]: /intrinsic/, /decentralised/, /cryptographically strong/ identifiers
#        + for /over 20 billion artifacts/
#        + [[https://www.softwareheritage.org/2020/05/13/swhid-adoption/][IANA registered, adopted in industry and WikiData]]
      - enhance articles with /source code references/ for reproducibility
#+BEAMER: \pause
*** Cite
      - [[https://www.ctan.org/tex-archive/macros/latex/contrib/biblatex-contrib/biblatex-software][biblatex-software]] : a dedicated bibliographic style for software!
#+BEAMER: \pause
*** 
    \hfill Detailed guidelines [[https://www.softwareheritage.org/save-and-reference-research-software/][are available online]]! \hfill \mbox{}
* Demo time!
** A walkthrough
   - Browse [[https://archive.softwareheritage.org][the archive]]
   - Get and use SWHIDs ([[https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html][full specification available online]])
   - cite software [[https://www.softwareheritage.org/2020/05/26/citing-software-with-style/][with the biblatex-software style]] from CTAN
   - Example use in a research article: compare Fig. 1 and conclusions 
      - in [[http://www.dicosmo.org/Articles/2012-DaneluttoDiCosmo-Pcs.pdf][the 2012 version]]
      - in [[https://www.dicosmo.org/share/parmap_swh.pdf][the updated version]] using SWHIDs and Software Heritage
   - Example use in a research article: extensive use of SWHIDs in [[https://www.dicosmo.org/Articles/2020-ReScienceC.pdf][a replication experiment]]
   - [[https://save.softwareheritage.org][Trigger archival]] of your preferred software in a breeze
   - [[https://doc.archives-ouvertes.fr/en/deposit/deposit-software-source-code/][curated deposit in SWH via HAL]], see for example: 
      [[https://hal.archives-ouvertes.fr/hal-02130801][LinBox]], [[https://hal.archives-ouvertes.fr/hal-01897934][SLALOM]], [[https://hal.archives-ouvertes.fr/hal-02130729][Givaro]], [[https://hal.archives-ouvertes.fr/hal-02137040][NS2DDV]], [[https://hal.archives-ouvertes.fr/lirmm-02136558][SumGra]], [[https://hal.archives-ouvertes.fr/hal-02155786][Coq proof]], ...
   - rescue landmark legacy software, see the [[https://www.softwareheritage.org/swhap/][SWHAP process with UNESCO]]
** Using the archive                                               :noexport:
*** Archiving (research) software
    - via HAL deposit, see for example: 
      [[https://hal.archives-ouvertes.fr/hal-02130801][LinBox]], [[https://hal.archives-ouvertes.fr/hal-01897934][SLALOM]], [[https://hal.archives-ouvertes.fr/hal-02130729][Givaro]], [[https://hal.archives-ouvertes.fr/hal-02137040][NS2DDV]], [[https://hal.archives-ouvertes.fr/lirmm-02136558][SumGra]], [[https://hal.archives-ouvertes.fr/hal-02155786][Coq proof]], ...
    - via [[https://archive.softwareheritage.org/browse/origin/save/][Save code now]]
*** Intrinsic identifiers for /reproducibility/
    - see the iPres 2018 article \url{bit.ly/swhpidpaper}
    - example: [[https://archive.softwareheritage.org/swh:1:cnt:41ddb23118f92d7218099a5e7a990cf58f1d07fa;origin=https://github.com/chrislgarry/Apollo-11;lines=53-82/][swh:1:cnt:41ddb23118f92d7218099a5e7a990cf58f1d07fa;lines=53-82]]
*** Wayback machine
    - find lost source code
    - example: [[https://archive.softwareheritage.org/browse/origin/https://gitorious.org/parmap/parmap.git/directory/][lost source code of Parmap cited in a 2012 research article]]
** Software Heritage for research, cont'd                          :noexport:
*** Describe
      - curated /deposit/: [[https://www.softwareheritage.org/2018/09/28/depositing-scientific-software-into-software-heritage/][moderated via *HAL*]], and via scholarly journals
      - software specific metadata via the [[https://codemeta.github.io/codemeta-generator/][CodeMeta generator]]
      #+BEAMER: \pause
*** Cite
      - working group on software citation
      - [[https://www.ctan.org/tex-archive/macros/latex/contrib/biblatex-contrib/biblatex-software][biblatex-software]] released to cite software in LaTeX bibliographies
      #+BEAMER: \pause
*** Reference archive :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    See the work done at https://swmath.org
    #+BEAMER: \pause
*** Landmark legacy software :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    See the [[https://www.softwareheritage.org/swhap/][SWHAP process with UNESCO]]
    #+BEAMER: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** 
   \hfill Now part of the /French National Plan for Open Science/ \hfill\mbox{}
* The way forward
** An international, non profit initiative\hfill built for the long term
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .5
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}{\includegraphics[width=\extblockscale{.4\linewidth}]{unesco_logo_en_285}}\end{center}
   #+LATEX: \vspace{-0.8cm}
   #+LATEX: \begin{center}\vskip 1em \includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
    #+latex: \small And many more ...\\
    #+latex:\mbox{}~~~~~~~\tiny\url{www.softwareheritage.org/support/testimonials}
*** Donors, members, sponsors                                       :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.4\linewidth}]{inria-logo-new}\end{center}
   #+LATEX: \begin{center}
   # #+LATEX: \includegraphics[width=\extblockscale{.2\linewidth}]{sponsors-levels.pdf}
   #+LATEX: \colorbox{white}{\includegraphics[width=\extblockscale{1.4\linewidth}]{sponsors.pdf}}
   #+LATEX: \end{center}
#   - sponsoring / partnership :: \hfill \url{sponsorship.softwareheritage.org}
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Research collaboration                              :B_picblock:noexport:
  :PROPERTIES:
  :BEAMER_COL: .5
  :BEAMER_env: picblock
  :BEAMER_OPT: pic=Qwant_Logo, leftpic=true
  :END:
    source code search engine
*** See more                                                       :noexport:
    \hfill\tiny\url{http:://www.softwareheritage.org/support/testimonials}
*** Global network                                      :B_picblock:noexport:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=fossid, leftpic=true, width=.3\linewidth
    :END:
     - first *independent mirror*
     - increased reliability
** Come in, we're open!
*** Software Heritage :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .42
    :END:
    - /universal/ source code archive
    - /intrinsic/ identifiers (SWHIDS)
    - /open/, /non profit/, long term
    - /infrastructure/ for Open Science
#+BEAMER: \pause
*** You can help improve science!                                   :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .55
    :END:
    - /use/ SWH and /save/ relevant source code
    - /build on/ SWH (see swmath.org and ipol.im) 
    - /contribute/ to SWH: /it is open source/
    - spread the word
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
  #+BEGIN_EXPORT latex
  \begin{thebibliography}{Foo Bar, 1969}
  \footnotesize
  \bibitem{Abramatic2018} Jean-François Abramatic, Roberto Di Cosmo, Stefano Zacchiroli\newblock
  \emph{Building the Universal Archive of Source Code}, CACM, October 2018
  \href{https://doi.org/10.1145/3183558}{(10.1145/3183558)}
  \bibitem{DiCosmo2019} Roberto Di Cosmo, Morane Gruenpeter, Stefano Zacchiroli\newblock
  \emph{Referencing Source Code Artifacts: a Separate Concern in Software Citation},\newblock
  CiSE 2020 \href{https://dx.doi.org/10.1109/MCSE.2019.2963148}{(10.1109/MCSE.2019.2963148)}
  \href{https://hal.archives-ouvertes.fr/hal-02446202}{(hal-02446202)}
  \bibitem{alliez:hal-02135891} Pierre Alliez, Roberto Di Cosmo, Benjamin Guedj, Alain Girault, Mohand-Said Hacid, Arnaud Legrand and  Nicolas Rougier\newblock
  \emph{Attributing and referencing (research) software: Best practices and outlook from Inria}, \newblock
  CiSE 2020 \href{https://doi.ieeecomputersociety.org/10.1109/MCSE.2019.2949413}{(10.1109/MCSE.2019.2949413)}
  \href{https://hal.archives-ouvertes.fr/hal-02135891}{(hal-02135891)}
  \end{thebibliography}
  #+END_EXPORT

* Appendix :B_appendix:
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
** Software and Software Source code
   #+INCLUDE: "../../common/modules/source-code-different-short.org::#thesourcecode" :only-contents t :minlevel 3
** Largest software archive, shared infrastructure
   #+latex: \begin{center}
   #+ATTR_LATEX: :width 0.7\linewidth
   file:SWH-as-foundation-slim.png
   #+latex: \end{center}
   #+BEAMER: \pause
   #+latex: \centering
   #+ATTR_LATEX: :width \extblockscale{.9\linewidth}
   file:2020-04-07-growth.png
** A revolutionary infrastructure for software source code
  #+BEAMER: \vspace{-2mm}
*** The /graph/ of Software Development                          :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_COL: .5
    :BEAMER_OPT: pic=git-merkle/merkle-vertical, leftpic=true, width=.4\linewidth
    :END:
    All software development with its history,
    in *a single graph* ...
 #+BEAMER: \pause \vspace{-2mm}
*** The /blockchain/ of Software Development                     :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=merkle, leftpic=true, width=.8\linewidth
    :BEAMER_env: picblock
    :BEAMER_COL: .5
    :BEAMER_act:
    :END:
    ... a single *Merkle* graph, with /intrinsic ids/ for *traceability*
 #+BEAMER: \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** A /pillar/ of Open Science                                   :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=PreservationTriangle.png,leftpic=true, width=.6\linewidth
    :BEAMER_COL: .5
    :END:
    Reference *archive* of\\
    Research Software
 #+BEAMER: \pause
*** Reference platform for /Big Code/                            :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=universal, leftpic=true, width=.4\linewidth
    :BEAMER_env: picblock
    :BEAMER_COL: .5
    :BEAMER_act:
    :END:
   *One uniform data structure* enables /massive/ machine learning
   for *quality, cybersecurity*, etc.

** Precious asset, /endangered heritage/                           :noexport:
*** We are at a turning point
    software *now a critical asset for society*, but key
    scientists/developers *are passing away*, and source code is getting *lost or
    misplaced* while software development *skyrockets*!
#+BEAMER: \pause
*** 
    To enable next generation research and software development we need \\
    \hfill{} a /common/, /non profit/, /long term/, /shared/ infrastructure that provides
*** A catalog
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.8\linewidth]{myriadsources}
\end{center}
#+END_EXPORT 
#+BEAMER: \pause
*** An archive
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .3
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.8\linewidth]{fragilecloud}
\end{center}
#+END_EXPORT 
#+BEAMER: \pause
*** A research infrastructure                                       :B_block:
    :PROPERTIES:
    :BEAMER_COL: .3
    :BEAMER_env: block
    :END:
#+BEGIN_EXPORT latex
\begin{center}
\includegraphics[width=.9\linewidth]{atacama-telescope}
\end{center}
#+END_EXPORT 
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
** Exponential growth and increasing complexity                    :noexport:
\vspace{-0.5em}
*** Growth of /globally original known content/ (source: [[https://hal.archives-ouvertes.fr/hal-02158292v2][Rousseau, Di Cosmo, Zacchiroli 2019]])
    #+ATTR_LATEX: :width .9\linewidth
    file:revision_content_growth_wide.png
    \vspace{-1em}
    trend over 20 years: /original content doubles every 22 months/
    (i.e. *~45% per year*)
#+BEAMER: \pause 
*** Complexity of the software ecosystem \hfill 8000+ langs      :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=python3-matplotlib.pdf, width=.5\linewidth
    :END:
    - /millions/ of lines of code
    - large /web of dependencies/
      + easy to break, difficult to maintain
    - decentralised /developer communities/
* Milestones
** Some key dates
   #+INCLUDE: "../../common/modules/swh-key-dates.org::#keydates" :minlevel 3 :only-contents t
** Policy highlight
*** :B_column:BMCOL:
    :PROPERTIES:
    :BEAMER_col: .53
    :BEAMER_env: column
    :END:
    #+ATTR_LATEX: :width .7\linewidth
    file:UNESCOParisCallMeeting.png
    UNESCO, Inria, Software Heritage invite\\
    [[https://en.unesco.org/news/experts-call-greater-recognition-software-source-code-heritage-sustainable-development][40 international experts meet in Paris]] ...
    #+BEAMER: \pause
*** :B_column:BMCOL:
    :PROPERTIES:
    :BEAMER_col: .5
    :BEAMER_env: column
    :END:
    #+ATTR_LATEX: :width .65\linewidth
    file:paris_call_ssc_cover.jpg
    [[https://en.unesco.org/foss/paris-call-software-source-code][Their call is published on Feb 2019]] \pause
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** 
    It's an important /policy tool/, already referenced and used ...
    \hfill /yes, you can sign it!/\\
    \vspace{10pt}
   \hfill https://en.unesco.org/foss/paris-call-software-source-code \hfill\mbox{}
    :PROPERTIES:
    :BEAMER_COL: 1.06
    :BEAMER_env: block
    :END:
* Breaking news
** Archiving /public/ code
   #+latex: \begin{center}
   #+ATTR_LATEX: :width 0.7\linewidth
   file:codeetalab.png
   #+latex: \end{center}
#+BEAMER: \pause
   https://code.etalab.gouv.fr
** ENEA mirror
*** Thomas Jefferson, February 18, 1791 :B_block:
    :PROPERTIES:
    :BEAMER_ACT:
    :BEAMER_env: block
    :END:      
#+latex:  {\em
  ...let us save what remains: not by vaults and locks which fence them
  from the public eye and use in consigning them to the waste of time,
  but by such a multiplication of copies, as shall place them beyond
  the reach of accident.
#+latex: }
   #+BEAMER: \pause
*** Welcoming ENEA                                                  :B_block:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=LogoENEAcompletoENG.png, leftpic=true, width=.7\linewidth
    :END:
     - first *institutional* mirror
     - increased resilience
     - *AI infrastructure* for researchers
     - stepping stone to \endgraf
       \hfill an European joint effort
** The Software Heritage Acquisition Process (SWHAP)
*** Paris Call on Software Source Code
    “[We call to] support efforts to gather and preserve the artifacts and
    narratives of the history of computing, while the earlier creators are still
    alive”
#+BEAMER: \pause
*** SWHAP : an important step forward
     - detailed guidelines to *curate* landmark legacy source code
       and *archive* it on Software Heritage
     - intense cooperation with *Università di Pisa* and *UNESCO*
     - open to all, we'll promote it worldwide
*** 
    https://www.softwareheritage.org/swhap
* A bit of tech info
** A peek under the hood                                          :maybemove:
    #+BEAMER: \begin{center}
    #+BEAMER:   \mode<beamer>{\only<1>{\includegraphics[width=\extblockscale{1\textwidth}]{swh-dataflow-merkle-listers.pdf}}}
    #+BEAMER:   \only<2-3>{\includegraphics[width=\extblockscale{1\textwidth}]{swh-dataflow-merkle.pdf}}
    #+BEAMER: \end{center}
#+BEAMER: \pause
#+BEAMER: \pause
   /Global development history/ permanently archived in a /unique/ git-like Merkle DAG
   - *~400 TB* (uncompressed) blobs, *~20 B* nodes, *~280 B* edges
   #   - *GitHub*, Gitlab.com, Bitbucket, /Gitorious/, /GoogleCode/, GNU, PyPi, Debian, NPM...
** Today: joining forces with GitHub                               :noexport:
*** Paris Call on Software Source Code\hfill UNESCO, February 2019
    [We call to] support: 
    - “efforts to gather and preserve the artifacts and narratives of the history of computing,
       while the earlier creators are still alive”
    - “a universal archive, as part of a broad effort at digital preservation, that will
       ensure persistence of and universal access to software source code”
#+BEAMER: \pause
*** Support SWH Acquisition Process                                 :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .45
    :END:
    #+ATTR_LATEX: :width .56\linewidth
    file:SWHAP.png
    # - SWH with UNESCO support
    # - GitHub: handle /old dates/
    \vspace{-0.2cm}
    \tiny \mbox{}\hfill https://www.softwareheritage.org/swhap
#+BEAMER: \pause
*** Better GitHub archival in SWH                                   :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .45
    :END:
    #+ATTR_LATEX: :width .9\linewidth
    file:GitHub-SWH-archive.png
    # - improve archival
    # - support effort
** You can help!                                                   :noexport:
*** Collect :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - /save source code now/!
    - help build a /shared process/
    - bootstrap the /focused search/
*** Preserve :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - host /ancillary material/
    - join /the SWH Foundation/
    - become a /mirror/
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Share :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - build /on top of/ SWH
*** Connect :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_COL: .5
    :END:
    - emulation, binaries, metadata, etc...
*** Awareness
    - leverage the [[https://en.unesco.org/foss/paris-call-software-source-code][Paris Call on Software Source Code]] (Unesco)
* The SWH-ID: the source code fingerprint                          :noexport:
** The SWH-ID schema
  # TODO: drawing with swh:1:cnt:xxxxxxx "exploded" and explained
  #+LATEX: \centering\forcebeamerstart
  #+LATEX: \only<1>{\includegraphics[width=\linewidth]{SWH-ID-1.png}}
  #+LATEX: \only<2>{\includegraphics[width=\linewidth]{SWH-ID-2.png}}
  #+LATEX: \only<3>{\includegraphics[width=\linewidth]{SWH-ID-3.png}}
  #+LATEX: \forcebeamerend
** A worked example
  #+LATEX: \centering\forcebeamerstart
  #+LATEX: \only<1>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_1.pdf}}}
  #+LATEX: \only<2>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/contents.pdf}}}
  #+LATEX: \only<3>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_2_contents.pdf}}}
  #+LATEX: \only<4>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/directories.pdf}}}
  #+LATEX: \only<5>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_3_directories.pdf}}}
  #+LATEX: \only<6>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/revisions.pdf}}}
  #+LATEX: \only<7>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_4_revisions.pdf}}}
  #+LATEX: \only<8>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/releases.pdf}}}
  #+LATEX: \only<9>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/merkle_5_releases.pdf}}}
  #+LATEX: \only<10>{\colorbox{white}{\includegraphics[width=\extblockscale{\linewidth}]{git-merkle/snapshots.pdf}}}
  #+LATEX: \forcebeamerend
** The Software Heritage ID schema \hfill (see *\url{http://bit.ly/swhpids}*)
#+BEGIN_EXPORT latex
\small
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:cnt:94a9ed024d3859793618152ea559a168bbcbb5e2}
{swh:1:{\bf cnt}:94a9ed024d3859793618152ea559a168bbcbb5e2} \hfill full text of the GPL3 license
\end{tcolorbox}
\pause
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:dir:d198bc9d7a6bcf6db04f476d29314f157507d505}
{swh:1:{\bf dir}:d198bc9d7a6bcf6db04f476d29314f157507d505} \hfill Darktable source code
\end{tcolorbox}
\pause
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:rev:309cf2674ee7a0749978cf8265ab91a60aea0f7d}
{swh:1:{\bf rev}:309cf2674ee7a0749978cf8265ab91a60aea0f7d}
\end{tcolorbox}
\hfill a {\bf revision} in the development history of Darktable\\\pause
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:rel:22ece559cc7cc2364edc5e5593d63ae8bd229f9f}
{swh:1:{\bf rel}:22ece559cc7cc2364edc5e5593d63ae8bd229f9f}
\end{tcolorbox}
\hfill {\bf release} 2.3.0 of Darktable, dated 24 December 2016\\\pause
\begin{tcolorbox}
\href{https://archive.softwareheritage.org/swh:1:snp:c7c108084bc0bf3d81436bf980b46e98bd338453}
{swh:1:{\bf snp}:c7c108084bc0bf3d81436bf980b46e98bd338453}
\end{tcolorbox}
\hfill a {\bf snapshot} of the entire Darktable repository (4 May 2017, GitHub)
#+END_EXPORT
#+LATEX: \pause
*** 
   *Current resolvers:* \url{archive.softwareheritage.org} and \url{n2t.org}
* The way forward
** An international, non profit initiative\hfill built for the long term
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .5
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}{\includegraphics[width=\extblockscale{.4\linewidth}]{unesco_logo_en_285}}\end{center}
   #+LATEX: \vspace{-0.8cm}
   #+LATEX: \begin{center}\vskip 1em \includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
    #+latex: \small And many more ...\\
    #+latex:\mbox{}~~~~~~~\tiny\url{www.softwareheritage.org/support/testimonials}
*** Donors, members, sponsors                                       :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.4\linewidth}]{inria-logo-new}\end{center}
   #+LATEX: \begin{center}
   # #+LATEX: \includegraphics[width=\extblockscale{.2\linewidth}]{sponsors-levels.pdf}
   #+LATEX: \colorbox{white}{\includegraphics[width=\extblockscale{1.4\linewidth}]{sponsors.pdf}}
   #+LATEX: \end{center}
#   - sponsoring / partnership :: \hfill \url{sponsorship.softwareheritage.org}
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Research collaboration                              :B_picblock:noexport:
  :PROPERTIES:
  :BEAMER_COL: .5
  :BEAMER_env: picblock
  :BEAMER_OPT: pic=Qwant_Logo, leftpic=true
  :END:
    source code search engine
*** See more                                                       :noexport:
    \hfill\tiny\url{http:://www.softwareheritage.org/support/testimonials}
*** Global network                                      :B_picblock:noexport:
    :PROPERTIES:
    :BEAMER_COL: .5
    :BEAMER_env: picblock
    :BEAMER_OPT: pic=fossid, leftpic=true, width=.3\linewidth
    :END:
     - first *independent mirror*
     - increased reliability
** CNRS and Software Heritage Working together
*** Resources
    - as a top SWH sponsor, contribute to the roadmap
      + funding (see https://sponsors.softwareheritage.org)
      + compute and storage infrastructure
      + engineers and researchers
*** Collaboration
    - training and adoption
      + across disciplines
      + at the international level 
    - strengthen SWH interoperability with HAL
*** Strategy
    - work together to establish an international infrastructure
#    - (multi)disciplinary research on SWH
** Come in, we're open!
#+BEGIN_EXPORT latex
% \begin{center}
% \includegraphics[width=.5\linewidth]{SWH-logo.pdf}
% \end{center}
% \begin{center}
% {\large \url{www.softwareheritage.org} \hspace{4em} \url{@swheritage}}
% \end{center}
#+END_EXPORT
*** Library of Alexandria of code                       :B_picblock:noexport:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_COL: 0.4
    :BEAMER_OPT: pic=clock-spring-forward.png,width=.45\linewidth,leftpic=true
    :END:
    Working together to
    - recover the past
    - preserve our heritage
    - share the knowledge
    - prepare the future
*** Library of Alexandria of code                          :B_block:noexport:
    :PROPERTIES:
    :BEAMER_COL: 0.42
    :BEAMER_env: block
    :END:
    \begin{center}\includegraphics[width=.4\linewidth]{clock-spring-forward.png}\end{center}
    - recover the past
    - structure the future
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
  #+BEGIN_EXPORT latex
  \vfill
  \begin{center}\Large Questions?\end{center}
  \vfill
  #+END_EXPORT
*** Learn more \hfill www.softwareheritage.org/publications         :B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:
  #+BEGIN_EXPORT latex
  \begin{thebibliography}{Foo Bar, 1969}
  \footnotesize

  \bibitem{ARSCSWH} Roberto Di Cosmo. \emph{Archiving and referencing source code with Software Heritage}\newblock
  ICMS 2020, preprint: hal-02526083

  \bibitem{Abramatic2018} Jean-François Abramatic, Roberto Di Cosmo, Stefano Zacchiroli\newblock
  \emph{Building the Universal Archive of Source Code}, Communications of the ACM, October 2018

  \bibitem{DiCosmo2020} P. Alliez, R. Di Cosmo, B. Guedj, A. Girault, M. Hacid, A. Legrand, N. Rougier\newblock
  \emph{Attributing and Referencing (Research) Software: Best Practices and Outlook From Inria},
  Computing in Science \& Engineering, 22 (1), pp. 39-52, 2020, ISSN: 1558-366X

  \bibitem{DiCosmo2020} Roberto Di Cosmo, Morane Gruenpeter, Stefano Zacchiroli\newblock
  \emph{Referencing Source Code Artifacts: a Separate Concern in Software Citation},
  Computing in Science \& Engineering, 2020, ISSN: 1521-9615

  \bibitem{OCamlP3l}  Roberto Di Cosmo, Marco Danelutto\newblock
  \emph{[Rp] Reproducing and replicating the OCamlP3l experiment}. ReScience C, 6(1), 2.

  \end{thebibliography}
  #+END_EXPORT
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Come in, we're open                                            :noexport:
    \hfill  https://sponsorship.softwarheritage.org
