#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Garantir un Accès Indépendant au Bien Commun Logiciel
#+BEAMER_HEADER: \date[2022-11-22, CNAM]{22 Novembre 2022\\Journée Infrastructures pour la Souveraineté Numérique\\CNAM, Paris, France\\[-2mm]}
#+AUTHOR: Stefano Zacchiroli
#+DATE: 22 Novembre 2022
#+EMAIL: stefano.zacchiroli@telecom-paris.fr

#+INCLUDE: "../../common/modules/prelude.org" :minlevel 1
#+INCLUDE: "../../common/modules/169.org"
#+BEAMER_HEADER: \institute[Télécom Paris]{Télécom Paris, Institut Polytechnique de Paris\\ {\tt stefano.zacchiroli@telecom-paris.fr}}
#+BEAMER_HEADER: \title[Un accès indépendant au bien commun logiciel]{Garantir un Accès Indépendant au Bien Commun Logiciel}
#+BEAMER_HEADER: \author{Stefano Zacchiroli}

* The Software Commons
** (Free) Software is everywhere in society
#+BEAMER: \centering
#+ATTR_LATEX: :width .7\linewidth
file:software-center.pdf

** The Commons and Free/Open Source Software
*** Definition (Commons)
The *commons* is the cultural and natural resources accessible to all members
of a society, including natural materials such as air, water, and a habitable
earth. These resources are held in common, not owned privately.
*** Definition (Software Commons)
The *software commons* consists of all computer software which is available at
little or no cost and which can be altered and reused with few
restrictions. Thus /all open source software and all free software are part of
the [software] commons/. […]
*** linebreak                                               :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
Kranich and Schement (2008); Schweik and English (2012).

* An Endangered Knowledge
** But /where/ is the software commons distributed from?
#+latex: \begin{flushleft}
#+ATTR_LATEX: :width \extblockscale{.68\linewidth}
file:myriadsources.png
#+latex: \end{flushleft}
*** 
- GitHub: 140 M repositories \hfill operated by: Microsoft, USA
- GitLab.com: 4 M \hfill GitLab, Inc., USA
- Bitbucket: 1.9 M \hfill Atlassian, Autralia
- NPM: 1.8 M packages \hfill Microsoft, USA

#+BEAMER: \footnotesize
source: [[https://archive.softwareheritage.org/][Software Heritage archive]]; total coverage: 188 M software origins (Nov. 2022)

** Software source code is fragile
#+latex: \begin{flushleft}
#+ATTR_LATEX: :width \extblockscale{.5\linewidth}
file:fragilecloud.png
#+latex: \end{flushleft}
*** Like all digital information, FOSS is fragile
# - inconsiderate and/or malicious code loss (e.g., Code Spaces)
- link rot: projects are created, moved around, removed
- business decisions (e.g., Gitorious, Google Code, Bitbucket)
- geopolitics (e.g., embargoes, "intellectual property" regulations, wars)
*** linebreak                                               :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
This matters to /you/ (even if you don't know about it).
One day it will show up at your door, most likely to break your software supply chain!

* Avoiding the Tragedy of an Unattended Software Commons
** Software Heritage in a nutshell \hfill www.softwareheritage.org
#+INCLUDE: "../../common/modules/swh-goals-oneslide-vertical.org::#goals" :only-contents t :minlevel 3

** The largest public source code archive, principled \hfill \small \url{bit.ly/swhpaper}
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.5
    :END:
    #+latex: \centering
    #+ATTR_LATEX: :width \linewidth
    file:SWH-as-foundation-slim.png
*** 
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.5
    :END:
    #+latex: \centering
    #+ATTR_LATEX: :width \linewidth
    file:archive-growth.png\\
    [[https://archive.softwareheritage.org][archive.softwareheritage.org]]
*** linebreak                                               :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \pause
*** Technology
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.34
    :END:
    - transparency and FOSS
    - replicas all the way down
*** Content (billions!)
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.32
    :END:
    - intrinsic identifiers
    - facts and provenance
*** Organization
    :PROPERTIES:
    :BEAMER_env: block
    :BEAMER_col: 0.33
    :END:
    - non-profit
    - multi-stakeholder
** An international, non profit initiative\hfill built for the long term
  :PROPERTIES:
  :CUSTOM_ID: support
  :END:
*** Sharing the vision                                              :B_block:
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :BEAMER_COL: .5
  :BEAMER_env: block
  :END:
   #+LATEX: \begin{center}{\includegraphics[width=\extblockscale{.4\linewidth}]{unesco_logo_en_285}}\end{center}
   #+LATEX: \vspace{-0.8cm}
   #+LATEX: \begin{center}\vskip 1em \includegraphics[width=\extblockscale{1.4\linewidth}]{support.pdf}\end{center}
   #+latex:\begin{center}\tiny\url{www.softwareheritage.org/support/testimonials}\end{center}
  # #+BEAMER: \pause
*** Donors, members, sponsors                                       :B_block:
    :PROPERTIES:
    :CUSTOM_ID: sponsors
    :BEAMER_COL: .5
    :BEAMER_env: block
    :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{.4\linewidth}]{inria-logo-new}\end{center}
   #+LATEX: \vspace{-1cm}
   #+LATEX: \begin{center}
   #+LATEX: \colorbox{white}{\includegraphics[width=\extblockscale{1.4\linewidth}]{sponsors.pdf}}
   #+LATEX: \end{center}
   #+latex:\begin{center}\tiny\url{www.softwareheritage.org/support/sponsors}\end{center}
*** :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
* Outlook
** Not the end of the story
#+BEAMER: \vspace{-1mm}
*** Takeaways
- Continued access to the entire public body of free/open source software is a
  strategic need for all of society, including both public and for-profit
  players.
- Software Heritage guarantees today an independent access to the largest
  publicly accessible share of the software commons.
- The archive is France-based and funded by a diverse set of international
  public/private and non-profit/for-profit actors.
*** linebreak                                               :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
#+BEAMER: \vspace{-2mm}
*** Discussion
- Depending on a FOSS component is more than needing its code at any given
  point in time (e.g., upgrades, support, community, etc.). How do we "archive"
  that?
- Funding: for true independence, we need to depend /less/ on non "local"
  funds. (Hello french IT companies!) But at the same time diversity is /good/
  for long-term survival.
- Is FOSS all the same, just because it /could/ be audited? Or do we care where
  contributions come from? (Geopolitics again!)

* Appendix                                                       :B_appendix:
  :PROPERTIES:
  :BEAMER_env: appendix
  :END:
** 
   \vfill
   \centerline{\Huge Appendix}
   \vfill

# #+INCLUDE: "../../common/modules/status-extended.org::#archivinggoals" :minlevel 2
# #+INCLUDE: "../../common/modules/status-extended.org::#architecture" :minlevel 2 :only-contents t
# #+INCLUDE: "../../common/modules/status-extended.org::#merkletree" :minlevel 2
# #+INCLUDE: "../../common/modules/data-model.org::#merklestruct" :minlevel 2
# #+INCLUDE: "../../common/modules/status-extended.org::#dagdetailsmall" :minlevel 2 :only-contents t

** Software /source code/ is precious human knowledge
#+INCLUDE: "../../common/modules/source-code-different-short.org::#softwareisdifferent" :only-contents t :minlevel 3

** A peek under the hood: a global view on the software commons
*** 
    #+BEAMER: \begin{center}
    #+BEAMER: \vspace{-2mm}
    #+BEAMER: \includegraphics[width=.8\textwidth]{swh-dataflow-merkle.pdf}
    #+BEAMER: \end{center}
    #+BEAMER: \pause
*** 
    #+BEAMER: \vspace{-2mm} \small
    A *global graph* linking together fully *deduplicated* source code artifact
    (files, commits, directories, releases, etc.) to the places that distribute
    them (e.g., Git repositories), providing a *unified view* on the entire
    */Software Commons/*.

    (Size: *~30 B* nodes, *~300 B* edges, *~1 PiB* blobs)
** Demo time!
   - Browse [[https://archive.softwareheritage.org][the archive]]
   - [[https://save.softwareheritage.org][Trigger archival]] of your preferred software in a breeze
   - Get and use SWHIDs ([[https://docs.softwareheritage.org/devel/swh-model/persistent-identifiers.html][full specification available online]])
   - The [[https://www.softwareheritage.org/2019/07/20/archiving-and-referencing-the-apollo-source-code/][Apollo 11 AGC source code example]]
   - Cite software [[https://www.softwareheritage.org/2020/05/26/citing-software-with-style/][with the biblatex-software style]] from CTAN
   - Example use in a research article: compare Fig. 1 and conclusions 
      - in [[http://www.dicosmo.org/Articles/2012-DaneluttoDiCosmo-Pcs.pdf][the 2012 version]]
      - in [[https://www.dicosmo.org/share/parmap_swh.pdf][the updated version]] using SWHIDs and Software Heritage
#   - Example use in a research article: extensive use of SWHIDs in [[https://www.dicosmo.org/Articles/2020-ReScienceC.pdf][a replication experiment]]
   - Example in a journal: [[http://www.ipol.im/pub/art/2020/300/][an article from IPOL]]
   - [[https://doc.archives-ouvertes.fr/en/deposit/deposit-software-source-code/][Curated deposit in SWH via HAL]], see for example: 
      [[https://hal.archives-ouvertes.fr/hal-02130801][LinBox]], [[https://hal.archives-ouvertes.fr/hal-01897934][SLALOM]], [[https://hal.archives-ouvertes.fr/hal-02130729][Givaro]], [[https://hal.archives-ouvertes.fr/hal-02137040][NS2DDV]], [[https://hal.archives-ouvertes.fr/lirmm-02136558][SumGra]], [[https://hal.archives-ouvertes.fr/hal-02155786][Coq proof]], ...
   - Rescue landmark legacy software, see the [[https://www.softwareheritage.org/swhap/][SWHAP process with UNESCO]]
** Academia & policy: growing adoption (selection)
   #+INCLUDE: "../../common/modules/swh-adoption-academic.org::#adoption" :only-contents t :minlevel 3   
