#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* Software Heritage for Scientific Publishing
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Save and reference research software \hfill \href{https://www.softwareheritage.org/save-and-reference-research-software/}{SWH guidelines}
  :PROPERTIES:
  :CUSTOM_ID: prepare
  :END:
*** Prepare your public repository with: :B_picblock:
    - README, LICENSE, AUTHORS & codemeta.json files
#+LATEX: \pause

*** What's a good README
\hfill extracted from \href{https://www.tldp.org/HOWTO/html_single/Software-Release-Practice-HOWTO/}{Eric Steven Raymond} and
\href{https://www.makeareadme.com/}{Make a README}

  /MUST/ include:
      - **Name** and a **description** of the software.
  #+BEAMER: \pause
  /SHOULD/ include:
      - how to **run** and **use** the source code
      - build **environment**, installation, requirements
 #+BEAMER: \pause
  /CAN/ include:
      - project **website** or **documentation** pointer and recent **news**
      - **visuals**


** Save and reference research software  \hfill \href{https://www.softwareheritage.org/save-and-reference-research-software/}{SWH guidelines}
:PROPERTIES:
:CUSTOM_ID: save
:END:
*** Save code now on \hfill \url{https://archive.softwareheritage.org/save/}
    - git, svn or mercurial
    - intrinsic metadata files
    - complete history


#+latex: \begin{center}
#+ATTR_LATEX: :width \linewidth
file:webui-save-code-now.png
#+latex: \end{center}

** Save and reference research software  \hfill \href{https://www.softwareheritage.org/save-and-reference-research-software/}{SWH guidelines}
:PROPERTIES:
:CUSTOM_ID: reference
:END:
  Choose the granularity level for the reference:

  #+BEAMER: \pause
*** file (with code fragment)
  #+BEGIN_EXPORT latex
  \begin{tcolorbox}
  \href{https://archive.softwareheritage.org/swh:1:cnt:c60366bc03936eede6509b23307321faf1035e23;lines=473-537;origin=https://github.com/sagemath/sage/}
  {swh:1:{\bf cnt}:c60366bc03936eede6509b23307321faf1035e23;lines=473-537}\\
  \mbox{} \hfill ... and add {\it ;origin=https://github.com/sagemath/sage/}
  \end{tcolorbox}
  \hfill James McCaffrey's {\bf algorithm} in sageMath
  #+END_EXPORT  
  #+BEAMER: \pause
*** directory 
  #+BEGIN_EXPORT latex
  \begin{tcolorbox}
  \href{https://archive.softwareheritage.org/swh:1:dir:c6f07c2173a458d098de45d4c459a8f1916d900f;origin=https://github.com/id-Software/Quake-III-Arena/}
  {swh:1:{\bf dir}:c6f07c2173a458d098de45d4c459a8f1916d900f}\\
  \mbox{}\hfill ... and add {\it ;origin=https://github.com/id-Software/Quake-III-Arena/}
  \end{tcolorbox}
  \hfill source code of {\bf Quake-III Arena} from id-Software
  #+END_EXPORT  
** Save and reference research software  \hfill \href{https://www.softwareheritage.org/save-and-reference-research-software/}{SWH guidelines}
:PROPERTIES:
:CUSTOM_ID: referencecontd
:END:
*** specific release
  #+BEGIN_EXPORT latex
  \begin{tcolorbox}
  \href{https://archive.softwareheritage.org/swh:1:rel:22ece559cc7cc2364edc5e5593d63ae8bd229f9f;origin=https://github.com/darktable-org/darktable/}
  {swh:1:{\bf rel}:22ece559cc7cc2364edc5e5593d63ae8bd229f9f}\\
  \mbox{}\hfill ... and add {\it ;origin=https://github.com/darktable-org/darktable/}
  \end{tcolorbox}
  \hfill {\bf release} 2.3.0 of Darktable, dated 24 December 2016
  #+END_EXPORT  
  #+BEAMER: \pause
*** full snapshot (including all branches and all releases) 
  #+BEGIN_EXPORT latex
  \begin{tcolorbox}
  \href{https://archive.softwareheritage.org/swh:1:snp:c7c108084bc0bf3d81436bf980b46e98bd338453;origin=https://github.com/darktable-org/darktable/}
  {swh:1:{\bf snp}:c7c108084bc0bf3d81436bf980b46e98bd338453}\\
  \mbox{}\hfill ... and add {\it ;origin=https://github.com/darktable-org/darktable/}
  \end{tcolorbox}
  \hfill a {\bf snapshot} of the entire Darktable repository (4 May 2017, GitHub)
  #+END_EXPORT
