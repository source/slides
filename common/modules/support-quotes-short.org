#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* Growing support
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** What they say of Software Heritage
   :PROPERTIES:
   :BEAMER_opt: allowframebreaks
   :END:      
*** Serge Abiteboul (french Academy of Science)
     # Software embodies a large part of all the technical and scientific knowledge
     # that is being developed today.

     Software Heritage will create ...
     # the largest public archive of software artifacts, which is 
     an essential infrastructure to grow and disseminate a universal body of knowledge.
*** Jean-Francois Abramatic (former W3C director)
     # Software is the key to universal access to our web of information.\\
     # Software has become an essential component of our daily lives.\\
     # Software must be collected, organized, preserved and shared.\\
     /We need the Software Heritage project/ to make this goal a reality.
*** Mike Milinkovich (Executive Director, Eclipse Foundation)
    Software is the fabric which binds our personal, social, industrial, and
    digital lives. [...]
    # It is an important part of our cultural heritage, as well as a
    # significant portion of the intellectual efforts of humanity since the
    # invention of the digital computer.
    The Eclipse Foundation is pleased to support this important effort to
    preserve and protect humanity's software legacy.
*** Allison Randal (President, Open Source Initiative)
    # The fundamental advantage of open source software is the freedom to use,
    # modify, and redistribute existing work, accelerating innovation into the
    # future.
    The Open Source Initiative is pleased to support the work of the Software
    Heritage project, preserving the treasure trove produced by 30 years of
    free and open source software development work, and making it accessible to
    future generations of developers and researche
*** John Sullivan (Executive Director, Free Software Foundation)
    # The freedom—and the ability—to build on the work of others is core to
    # free software. Also central is the ability to be nimble and future-proof,
    # to not be hostage to the whims of specific owners or authors. The
    # relatively short history of computers already holds a tragically large
    # graveyard of software and formats buried too deep for anyone to learn
    # from.
    A comprehensive, permanent archive of free software like Software Heritage
    is exactly what we need to make all current [free software] works available
    as a foundation for the constructions of the future.
*** Nenad Medvidovic (Chair, ACM Sigsoft)
    # Software is now present everywhere. It is a pillar of our modern
    # societies, and the source code of this software embodies our scientific
    # and technological knowledge. In a word, it is our DNA.
    We fully support the objective of the Software Heritage project to create a
    comprehensive public archive of software artifacts.
*** Diomidis Spinellis (Editor-in-Chief, IEEE Software)
    I wish to express my wholehearted support for the Software Heritage
    project.
    # The vision of IEEE Software is building the community of leading software
    # practitioners. The long-term preservation of our community’s work and
    # achievements can be an important element for realizing our vision.
*** Carlo Ghezzi (President, Informatics Europe)
    We would like to express Informatics Europe’s support to the “Software
    Heritage” project.
    # Informatics Europe fully support the projects’ mission of collecting,
    # organising, preserving and sharing all the software that lies at the
    # heart of our culture and our society by creating a largest, curated, and
    # public archive of software artifacts, together with extensive metadata.
*** Simon Phipps (former president, Open Source Initiative)
    # Open source software is now the assumed default for the IT industry and
    # for every computer user. It is also here to stay for the foreseeable
    # future.
    We now need to secure past open source achievements, not only because they
    are part of human history, but also because they will come in handy
    centuries from now to read today's data.  The Software Heritage project
    will be the key actor in making such preservation possible.
