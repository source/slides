#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1

* Analysis of the Digital Object Identifier
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** The Digital Object Identifier (DOI)
  :PROPERTIES:
  :CUSTOM_ID: doiexplained
  :END:

*** Example: doi:10.1109/MSR.2015.10 				 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=doi-sample.png, width=.4\linewidth
    :END:
    - to find what 10.1109/MSR.2015.10 is, go to a /resolver/ (e.g. doi.org)
# (e.g.: http://dx.doi.org/10.1109/MSR.2015.10)
    - this returns http://ieeexplore.ieee.org/document/7180064/
    - at this URL we find ...
*** Architecture of the DOI infrastructure 			 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=DOI.png, width=.4\linewidth, leftpic=true
    :END:
#+BEAMER: \pause
    - DOI resolution /can change/
    - content at URL /can change/
    - no /intrinsic/ way of noticing
    - persistence based on /good will/ of /multiple parties/
** Real world examples...
*** An image from /figshare/					 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=doi-missing-entry-source.png, width=.5\linewidth
    :END:
*** 
    \hfill let's click on the DOI...
** ... may indeed not work
*** DOI not found						 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=doi-missing-entry-report.png, width=.7\linewidth
    :END:
** Good citizen
*** Yes, we report broken links/dois 				 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=doi-missing-entry.png,width=.9\linewidth
    :END:
