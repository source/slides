#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Preserving and sharing the technological knowledge of mankind
#+AUTHOR: Roberto Di Cosmo
#+DATE: 9/11/2015
#+EMAIL: roberto@dicosmo.org
#+DESCRIPTION: Preserving the technological knowledge of mankind
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology
#+BEAMER_HEADER_EXTRA: \title[Preserving Knowledge]{Preserving and sharing the technological knowledge of mankind}

#
# Prelude contains all the information needed to export the main beamer latex source
#

#+INCLUDE: "prelude.org" :minlevel 1

#
# why software is important (2 slides)
#
#+INCLUDE: "software-all-around-us.org::#main" :minlevel 1

#
# what SWH does (mission, collect protect share, basic infrastructure schema)
#
#+INCLUDE: "swh-overview.org::#main" :minlevel 1

#
# Key design questions
#
#+INCLUDE: "swh-design.org" :minlevel 1

#
# Better society, better industry, better science
# 
#+INCLUDE: "vision.org::#main" :minlevel 1

#
# How we want to work
#
#+INCLUDE: "bits-drawing-board.org::#main" :minlevel 1

#
# SWH and other international institutions
#
#+INCLUDE: "collaboration.org::#main" :minlevel 1

#
# What they say of SWH
#
#+INCLUDE: "support-quotes.org::#main" :minlevel 1

#
# What they say of SWH
#
#+INCLUDE: "support-quotes-prospects.org::#main" :minlevel 1
