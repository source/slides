# aspect ratio can be changed, but the slides need to be adapted
# - compute a "resizing factor" for the images (macro for picblocks?)
#
# set the background image
#
#+BEAMER_HEADER: \pgfdeclareimage[height=96mm,width=128mm]{bgd}{swh-world.png}
#+BEAMER_HEADER: \setbeamertemplate{background}{\pgfuseimage{bgd}}

#+LaTeX_CLASS_OPTIONS: [aspectratio=43,xcolor=table]

#+latex: \extblocksetscale{0.7}
