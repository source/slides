#+BEAMER_HEADER: \titlegraphic{\includegraphics[width=\extblockscale{0.7\textwidth}]{SWH-logo+motto}}

#+STARTUP: hidestars
# activate org-beamer-mode minor mode automatically
#+STARTUP: beamer

# org export options
#+LANGUAGE:  en
#+OPTIONS:   H:2 num:t toc:nil \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:
#+LINK_HOME:
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [presentation,xcolor=table]

#
# important font choice!
#
#+LaTeX_HEADER: \usepackage{libertine}

#
# Let's move that logo...
#
#+LaTeX_HEADER: \usepackage{animate}

#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)

# have the theme desired
#+latex_header: \mode<presentation>{\usetheme{swh} \beamertemplatenavigationsymbolsempty \setbeamertemplate{navigation symbols}{} \setbeamertemplate{headline}{}
#+latex_header: \setbeamertemplate{footline}
#+latex_header: {
#+latex_header:   \leavevmode%
#+latex_header:   \hbox{%
#+latex_header:   \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
#+latex_header:     \usebeamerfont{author in head/foot}\insertshortauthor%~~\beamer@ifempty{\insertshortinstitute}{}{(\insertshortinstitute)}
#+latex_header:   \end{beamercolorbox}%
#+latex_header:   \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.25ex,dp=1ex,right]{title in head/foot}%
#+latex_header:     \usebeamerfont{title in head/foot}\insertshorttitle{}\hspace*{2em}
#+latex_header:     \usebeamerfont{date in head/foot}\insertshortdate{}\hspace*{2em}
#+latex_header:     \insertframenumber{} / \inserttotalframenumber\hspace*{2ex} 
#+latex_header:   \end{beamercolorbox}}%
#+latex_header:   \vskip0pt%
#+latex_header: }
#+latex_header: }

# some color
# #+latex_header: \rowcolors[]{1}{blue!10}{blue!05}  # no longer works with texlive >= 2022

# set the paths for images
#+latex_header: \graphicspath{%
#+latex_header:               {../../common/images/}{../../common/logos/}%
#+latex_header:               {pics/}{../images/}{../../images/}{../pics/}{../../pics/}%
#+latex_header:               {../figures/}{../../figures/}{../logos/}{../../logos/}{../../../logos/}%
#+latex_header:               {../../communication/web/graphics/carousel/}%
#+latex_header:               {../../communication/web/graphics/pictos/png/400x400/}%
#+latex_header:  }
# some default information I did not find how to set this in org-mode

# to add the picblock macro
#+latex_header: \usepackage{extblocks}
#+latex_header: \usepackage{pgfpages}
#+latex_header: \usepackage{animate}
#+latex_header: \usepackage{alltt}
#
# Itemize in multiple columns
#
#+latex_header: \usepackage{multicol}
#
# Requires
#
# http://www-ljk.imag.fr/membres/Jerome.Lelong/latex/appendixnumberbeamer.sty
#+latex_header: \usepackage{appendixnumberbeamer}

#
# Colors, color boxes
#
#+latex_header: \usepackage{color}
#+latex_header: \usepackage{soul}

# http://tex.stackexchange.com/questions/41683/why-is-it-that-coloring-in-soul-in-beamer-is-not-visible
#+latex_header: \makeatletter
#+latex_header: \newcommand\SoulColor{%
#+latex_header:   \let\set@color\beamerorig@set@color
#+latex_header:   \let\reset@color\beamerorig@reset@color}
#+latex_header: \makeatother
#+latex_header: \SoulColor

#+LATEX_HEADER: \usepackage{listings}
#+LATEX_HEADER: \usepackage{forcebeamermode}

#
# Color of links
#
#+LATEX_HEADER: \hypersetup{colorlinks,linkcolor=,urlcolor=cyan}
