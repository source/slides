#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
** Software Heritage Identifiers (SWHIDs) \hfill ([[https://www.swhid.org/][swhid.org]])
  :PROPERTIES:
  :CUSTOM_ID: oneslide
  :END:
   #+LATEX: \centering
   #+LATEX: \mode<beamer>{\only<1>{\includegraphics[width=\linewidth]{SWHID-v1.4_1.png}}}
   #+LATEX: \mode<beamer>{\only<2>{\includegraphics[width=\linewidth]{SWHID-v1.4_2.png}}}
   #+LATEX: \only<3->{\includegraphics[width=\linewidth]{SWHID-v1.4_3.png}}
   #+LATEX: \forcebeamerend \vspace{-6mm}
*** An emerging standard :B_block:
    :PROPERTIES:
    :BEAMER_act: <4->
    :BEAMER_COL: .6
    :BEAMER_env: block
    :END:
    - Adoption: [[https://spdx.github.io/spdx-spec/appendix-VI-external-repository-identifiers/#persistent-id][SPDX 2.2]], IANA-registered ="swh:"= URI prefix, WikiData [[https://www.wikidata.org/wiki/Property:P6138][P6138]], …
    - Breaking news: *ISO standardization*
*** Examples :B_block:
    :PROPERTIES:
    :BEAMER_act: <5->
    :BEAMER_COL: .4
    :BEAMER_env: block
    :END:
    - [[https://archive.softwareheritage.org/swh:1:cnt:64582b78792cd6c2d67d35da5a11bb80886a6409;origin=https://github.com/virtualagc/virtualagc;visit=swh:1:snp:3c074afad81ad6b14d434b96e705e01d184752cf;anchor=swh:1:rev:007c2b95f301f9438b8b74d7993b7a3b9a66255b;path=/Luminary099/THE_LUNAR_LANDING.agc;lines=245-261/][Apollo 11 AGC excerpt]] 
    - [[https://archive.softwareheritage.org/swh:1:cnt:bb0faf6919fc60636b2696f32ec9b3c2adb247fe;origin=https://github.com/id-Software/Quake-III-Arena;visit=swh:1:snp:4ab9bcef131aaf449a7c01370aff8c91dcecbf5f;anchor=swh:1:rev:dbe4ddb10315479fc00086f08e25d968b4b43c49;path=/code/game/q_math.c;lines=549-572/][Quake III rsqrt]] 

* The SWHID: the source code fingerprint
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** The SWHID schema
  :PROPERTIES:
  :CUSTOM_ID: swh-id-syntax
  :END:
  #+LATEX: \centering\forcebeamerstart
  #+LATEX: \only<1>{\includegraphics[width=\linewidth]{SWH-ID-1.png}}
  #+LATEX: \only<2>{\includegraphics[width=\linewidth]{SWH-ID-2.png}}
  #+LATEX: \only<3>{\includegraphics[width=\linewidth]{SWH-ID-3.png}}
  #+LATEX: \forcebeamerend

* Compute your own SWHIDs \hfill \href{https://pypi.org/project/swh.model/}{pypi.org/project/swh.model}
  :PROPERTIES:
  :CUSTOM_ID: swhidentify
  :END:
** 
  #+BEAMER: \footnotesize
  #+BEGIN_SRC
$ pip install swh-model[cli]

$ swh identify fork.c kmod.c sched/deadline.c
swh:1:cnt:2e391c754ae730bd2d8520c2ab497c403220c6e3  fork.c
swh:1:cnt:0277d1216f80ae1adeed84a686ed34c9b2931fc2  kmod.c
swh:1:cnt:57b939c81bce5d06fa587df8915f05affbe22b82  sched/deadline.c

$ swh identify --no-filename /usr/src/linux/kernel/
swh:1:dir:f9f858a48d663b3809c9e2f336412717496202ab

$ git clone --mirror \
    https://forge.softwareheritage.org/source/helloworld.git
$ swh identify --type snapshot helloworld.git/
swh:1:snp:510aa88bdc517345d258c1fc2babcd0e1f905e93  helloworld.git
#+END_SRC
  #+BEAMER: \normalsize

** Warning
   If you expect /others/ to be able to resolve the SWHIDs of source code you
   care about, you should make sure the corresponding software is archived in
   Software Heritage.
