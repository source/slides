#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* The identifiers arena
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** The identifiers arena
#+latex: \begin{center}
   #+ATTR_LATEX: :width \linewidth
file:identifiers-arena.png
#+latex: \end{center}
