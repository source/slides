#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* Endorsement
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Sharing the Software Heritage vision
  :PROPERTIES:
  :CUSTOM_ID: endorsement
  :END:
   #+LATEX: \begin{center}\includegraphics[width=\extblockscale{\linewidth}]{support.pdf}\end{center}
*** See more
    \hfill\tiny\url{http:://www.softwareheritage.org/support/testimonials}
