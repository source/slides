#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* We would like Github to participate
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** An ideal win-win partnership
   #+LATEX: \includegraphics[width=\extblockscale{.3\linewidth}]{action-testimonial__red.png}
*** Collaboration with SWH means
     - contributing to the larger social good
     - greater visibility outside the usual developer circle
     - long term preservation guarantee for GitHub hosted projects
     - access to breakthrough technology built for Software Heritage
** In practice
*** Technical collaboration					 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=technical.png,leftpic=true,width=.1\linewidth
    :END:
    - low latency updates for Software Heritage
#    - make available GitHub project metadata for archival
    - become a Software Heritage mirror node 
#+BEAMER: \pause
*** Legal and policy collaboration				 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=legal.png, leftpic=true, width=.1\linewidth
    :END:
    - terms of service: explicit provisions for archival
    - cooperate towards archival exceptions in general
#+BEAMER: \pause
*** Financial							 :B_picblock:
    :PROPERTIES:
    :BEAMER_env: picblock
    :BEAMER_opt: pic=donate.png, leftpic=true, width=.1\linewidth
    :END:
    Help us build an endowment fund to
    - accelerate development and take up of Software Heritage
    - facilitate the creation of an international non profit organisation, like the W3C
