#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+INCLUDE: "prelude.org" :minlevel 1
* Next steps
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** The next steps
*** Founding partners					      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.45
    :BEAMER_env: block
    :END:
    - philanthropists
    - foundations
    - IT companies
    - ...
*** International organisations				      :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.45
    :BEAMER_env: block
    :BEAMER_act: +-
    :END:
    - Unesco
      + digital heritage
      + education
      + science
      + knowledge society
*** Two key milestones
    :PROPERTIES:
    :BEAMER_act: +-
    :END:
    - Public announcement
      + Q2 2016
      + with the co-founders
    - Creation of the nonprofit legal entity
      + one to two years
      + co-founders participate in defining the governance
