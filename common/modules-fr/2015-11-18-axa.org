#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#+TITLE: Préserver et diffuser la connaissance technologique de l'Humanité
# does not allow short title, so we override it for beamer as follows :
#+BEAMER_HEADER: \title[Presentation de Software Heritage]{Préserver et diffuser la connaissance technologique de l'Humanité}
#+AUTHOR: Roberto Di Cosmo
#+DATE: 18 Nov 2015
#+EMAIL: roberto@dicosmo.org
#+DESCRIPTION: Preserving the technological knowledge of mankind
#+KEYWORDS: software heritage legacy preservation knowledge mankind technology

#
# Prelude contains all the information needed to export the main beamer latex source
#

#+INCLUDE: "prelude.org" :minlevel 1

#
# why software is important (2 slides)
#
#+INCLUDE: "software-all-around-us.org::#main" :minlevel 1

#
# what SWH does (mission, collect protect share, basic infrastructure schema)
#
#+INCLUDE: "swh-overview.org::#main" :minlevel 1

#
# Better society, better industry, better science
# 
#+INCLUDE: "vision.org::#main" :minlevel 1

#
# How we want to work
#
# no export #+INCLUDE: "bits-drawing-board.org::#main" :minlevel 1

#
# une perspective de long terme
#
#+INCLUDE: "long-term.org::#main" :minlevel 2
#

#
# What they say of SWH
#
#+INCLUDE: "support-quotes-short.org::#main" :minlevel 1

#
# Unesco
#
#+INCLUDE: "collaboration.org::#main" :minlevel 1

#
# Avancement du projet
#
#+INCLUDE: "swh-status.org::#main" :minlevel 1
* Venez nous rejoindre
** Nous invitons le fonds Axa à rejoindre Software Heritage
   Rôle d'un co-fondateur
*** Contribuer à un fond d'amorçage
   - accelerer le dévéloppement et l'adoption
   - accompagner le projet pour les premiers 3 à 5 ans
*** Contribuer à l'organisation						   
    - création de la non for profit
    - dévéloppement du reseau international
*** Contribuer à la communication			    
    - workshop(s) sur Software Heritage
    - conférence internationale sur la préservation du logiciel
* Contact
** Pour nous contacter
   /email:/ roberto@dicosmo.org\vfill
   /upcoming website:/ http://www.softwareheritage.org
