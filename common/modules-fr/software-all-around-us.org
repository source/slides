#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
#
# Software is all around us
#
#+INCLUDE: "prelude.org" :minlevel 1
* Le logiciel autour de nous
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Le logiciel est pervasif
  :PROPERTIES:
  :CUSTOM_ID: softwareispervasive
  :END:
*** Au coeur de /notre société/ 				 :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=software-center.pdf, leftpic=true, width=.4\linewidth
    :BEAMER_env: picblock
    :END:
#+ATTR_BEAMER: :environment itemize
- communication, divertissement
- administration, finance
- santé, energie, transport
- recherche, education, politique
- ...
# #+BEAMER: \pause
*** /Médiateur/ pour accéder à /toutes/ les informations (c) Banski :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=Banski-Information-Pillar-Society-Small
    :BEAMER_env: picblock
    :END:

L'information est un *pilier* des nos sociétés modernes.
#+BEGIN_QUOTE
Absent an ability to correctly interpret digital information, we are left
with [...] "rotting bits" [...] of no value.

\mbox{}\hfill Vinton G. Cerf IEEE 2011
#+END_QUOTE

** Le logiciel est Connaissance
  :PROPERTIES:
  :CUSTOM_ID: softwareisknowledge
  :END:
*** Le logiciel est /un composant essentiel/ de la recherche scientifique :B_picblock:
#    Deep knowledge embodied in complex software systems
    :PROPERTIES:
    :BEAMER_opt: pic=papermountain,width=.15\linewidth
    :BEAMER_env: picblock
    :BEAMER_act: +-
    :END:      
#+BEGIN_QUOTE
[...] the vast majority describe experimental methods or sofware that have become essential in their fields.\\
#+END_QUOTE
\mbox{}\hfill Top 100 papers (Nature, October 2014)
# http://www.nature.com/news/the-top-100-papers-1.16224

*** Au coeur de la /technologie/ 				 :B_picblock:
    :PROPERTIES:
    :BEAMER_opt: pic=pervasiveComputing, leftpic=true
    :BEAMER_env: picblock
    :BEAMER_act: +-
    :END:
#+ATTR_BEAMER: :environment itemize
- appareils domestiques $\approx$ 10M SLOC
- téléphones $\approx$ 20M, /voitures/ $\approx$ 100M SLOC
- Internet of things, ...

*** Le Logiciel contient notre /Connaissance/ et notre /Patrimoine Culturel/
    :PROPERTIES:
    :END:      

Nous /devons/ le /recolter/, /préserver/, /reférencer/ et le rendre /accessible/:
nos /vies/, notre /industrie/, notre /recherche/, notre /société/ en dependent!

** Le code source est /spécial/
  :PROPERTIES:
  :CUSTOM_ID: softwareisdifferent
  :END:
   #+LATEX: \includegraphics[width=\extblockscale{.15\linewidth}]{software.png}
#+BEGIN_QUOTE  
 “Programs must be written for people to read, and only incidentally for machines to execute.”
   Harold Abelson, Structure and Interpretation of Computer Programs 
#+END_QUOTE
*** Distinguishing features
    - connaissance /executable/ et /qui pour être lue/ 
      + même le matériel est ... logiciel! (VHDL, FPGA, ...)
      + /les formats texte ne changent pas souvent/
    - il /evolues/ naturellement
      +  /l'histoire de dévéloppement/ est importante pour /comprendre/
    - complexe: beaucoup /de dependences/, millions de lignes de code
*** En quelques mots
    - le logiciel /n'est pas/ une sequence of bits comme les autres
    - un archive du logiciel /n'est pas/ un archive comme les autres
