#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %10BEAMER_act(Act) %4BEAMER_col(Col) %10BEAMER_extra(Extra) %8BEAMER_opt(Opt)
#
# Software is all around us
#
#+INCLUDE: "prelude.org" :minlevel 1
* Software source code
  :PROPERTIES:
  :CUSTOM_ID: main
  :END:
** Le code source d'un logiciel est /special/
  :PROPERTIES:
  :CUSTOM_ID: softwareisdifferent
  :END:
*** Harold Abelson, Structure and Interpretation of Computer Programs
    /“Programs must be written for people to read, and only incidentally for machines to execute.”/

*** Quake 2 (extrait)                                         :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.45
    :BEAMER_env: block
    :END:
   #+LATEX: \includegraphics[width=\linewidth]{quake-carmack-sqrt-1.png}
   # smart efficient implementation of 1/sqrt(x) on a CPU without special support
*** Network queue, Linux (extrait)                            :B_block:BMCOL:
    :PROPERTIES:
    :BEAMER_col: 0.45
    :BEAMER_env: block
    :END:
   #+LATEX: \includegraphics[width=\linewidth]{juliusz-sfb-short.png}
   # Juliusz implementation of stochastic fair blue in the Linux Kernel linux/net/sched/sch_sfb.c

*** 							    :B_ignoreheading:
    :PROPERTIES:
    :BEAMER_env: ignoreheading
    :END:
*** Len Shustek, Computer History Museum
    \hfill /“Source code provides a view into the mind of the designer.”/
   
*** Distinguishing features					   :noexport:
    - /executable/ and /human readable/ knowledge (an /all time new/)
      + even hardware is... software! (VHDL, FPGA, ...)
      + /text files are forever/
    - naturally /evolves/ over time
      + the /development history/ is key to its /understanding/
    - complex: large /web of dependencies/, millions of SLOCs
*** In a word							   :noexport:
    - software /is not just another/ sequence of bits
    - a software archive /is not just another/ digital archive

