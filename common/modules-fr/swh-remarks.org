#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)

** Le logiciel, notre responsabilité d'Informaticies
*** Beaucoup d'initiatives sur la préservation
    - RDA
    - Zenodo
    - Internet Archive
*** Le logiciel est différent
    - interdépendences
    - évolution ramifiée et rapide
*** mais faisons bref
    - voyons juste le problème de la préservation /en général/

