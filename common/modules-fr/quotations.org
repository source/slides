#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
* other quotes
** Thomas Jefferson
    “Let us save what remains: not by vaults and locks which fence them from the
    public eye and use in consigning them to the waste of time, but by such a
    multiplication of copies, as shall place them beyond the reach of accident.”
