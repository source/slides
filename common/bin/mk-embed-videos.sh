#!/usr/bin/env bash

# Usage:
#   ./make-posters.sh video1.mp4 video2.mov ...
# For each video passed in, this script:
#   1) Creates a poster image from the first frame, named base-poster.png
#   2) Generates a LaTeX snippet in base-embedvideo.tex
#      containing a \href{run:...}{\includegraphics{...}} block.

if [ $# -eq 0 ]; then
  echo "Usage: $0 video1 [video2 ...]"
  echo "No input videos given."
  exit 1
fi

for video in "$@"; do
  # Strip the extension from the video filename
  base="${video%.*}"

  # Define the poster and snippet filenames
  poster="${base}-poster.png"
  snippet="${base}-embedvideo.tex"

  echo "Processing '$video'..."

  # 1. Create a poster image (the first frame) using ffmpeg
  #    -y        Overwrite existing file without prompting
  #    -ss 0     Seek to timestamp 0 (the very first frame)
  #    -vframes 1   Extract only 1 frame
  # ffmpeg -y -ss 0 -i "$video" -vframes 1 "$poster"
  ffmpeg -hide_banner -loglevel warning -y -ss 0 -i "$video" -update 1 -frames:v 1 "$poster"
  # 2. Create the LaTeX snippet file
  cat <<EOF > "$snippet"
% Auto-generated snippet for embedding '$video' in pdfpc
% Poster image: '$poster'
%
% Sample usage (in your main .tex document):
% \begin{frame}
%   \input{$snippet}
% \end{frame}
\begin{center}
\href{run:${video}?poster=${poster}}{%
  \includegraphics[width=.8\linewidth, keepaspectratio]{${poster}}%
}
\end{center}
EOF

  echo "  -> Created poster image: $poster"
  echo "  -> Created LaTeX snippet: $snippet"
  echo
done
