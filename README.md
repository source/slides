# slides

## requirements

- latex dependencies (debian-like system):

  sudo apt install emacs-nox rubber texlive-latex-extra texlive-fonts-extra

- org-mode >= 8.3 at least for proper export with allowframebreaks to work

- to export in LaTeX format using the beamer class, you will need the following in your
  Emacs configuration:

    (require 'ox-beamer)
    (add-to-list 'org-beamer-environments-extra
                 '("picblock" "P"
                   "\\begin{picblock}%o{%h}"
                   "\\end{picblock}"))

## how to use

Inside talks-public folder, you will find sources for existing presentation. Copy one
that matches your current new presentation then hack on it.

Once ready, just go inside the folder of your new presentation and compile it (`make`).
